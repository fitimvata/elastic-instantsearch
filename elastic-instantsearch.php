<?php
/**
 * Plugin Name:     Elastic Instantsearch
 * Plugin URI:      https://newmedia.al/
 * Description:     Elastic Instantsearch
 * Author:          Fitim Vata
 * Author URI:      https://newmedia.al
 * Text Domain:     elastic-instantsearch
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Elastic_Instantsearch
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}


define( 'ELASTIC_INSTANTSEARCH_VERSION', '0.1.0' );
define( 'ELASTIC_INSTANTSEARCH_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
define( 'ELASTIC_LOGO_WHITE', 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGlkPSJMYXllcl8xIiBkYXRhLW5hbWU9IkxheWVyIDEiIHZpZXdCb3g9IjAgMCA3MyA4MCI+ICA8cGF0aCBmaWxsPSIjZmZmIiBkPSJNOCA0MGEzMiAzMiAwIDAgMCAxLjA1IDhINDhhOCA4IDAgMCAwIDgtOCA4IDggMCAwIDAtOC04SDkuMDlBMzIgMzIgMCAwIDAgOCA0MHpNNjEuODkgMjMuMzJBMjkuNTkgMjkuNTkgMCAwIDAgNjUgMjBhMzEuOTIgMzEuOTIgMCAwIDAtNTMuNjcgNmg0My43M2ExMC4wNyAxMC4wNyAwIDAgMCA2LjgzLTIuNjh6TTU1LjA2IDU0SDExLjMzQTMxLjkyIDMxLjkyIDAgMCAwIDY1IDYwYTI5LjU5IDI5LjU5IDAgMCAwLTMuMTEtMy4zMkExMC4wNyAxMC4wNyAwIDAgMCA1NS4wNiA1NHoiIC8+PC9zdmc+'); // phpcs:ignore
define( 'ELASTIC_LOGO', 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA3MyA4MCI+ICA8cGF0aCBmaWxsPSIjMzQzNzQxIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik04IDQwYTMyIDMyIDAgMCAwIDEuMDUgOEg0OGE4IDggMCAwIDAgOC04IDggOCAwIDAgMC04LThIOS4wOUEzMiAzMiAwIDAgMCA4IDQweiIvPiAgPHBhdGggZmlsbD0iI2ZlYzUxNCIgZmlsbC1ydWxlPSJldmVub2RkIiBkPSJNNjEuODkgMjMuMzJBMjkuNTkgMjkuNTkgMCAwIDAgNjUgMjBhMzEuOTIgMzEuOTIgMCAwIDAtNTMuNjcgNmg0My43M2ExMC4wNyAxMC4wNyAwIDAgMCA2LjgzLTIuNjh6Ii8+ICA8cGF0aCBmaWxsPSIjMDBiZmIzIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik01NS4wNiA1NEgxMS4zM0EzMS45MiAzMS45MiAwIDAgMCA2NSA2MGEyOS41OSAyOS41OSAwIDAgMC0zLjExLTMuMzJBMTAuMDcgMTAuMDcgMCAwIDAgNTUuMDYgNTR6Ii8+PC9zdmc+'); // phpcs:ignore

if ( ! defined( 'ELASTIC_INSTANTSEARCH_PATH' ) ) {
	define( 'ELASTIC_INSTANTSEARCH_PATH', plugin_dir_path( __FILE__ ) );
}

/**
 * I18n.
 */
function elastic_instantsearch_load_textdomain() {
	$locale = apply_filters( 'plugin_locale', get_locale(), 'elastic-instantsearch' );

	load_textdomain( 'elastic-instantsearch', WP_LANG_DIR . '/elastic-instantsearch/elastic-instantsearch-' . $locale . '.mo' );
	load_plugin_textdomain( 'elastic-instantsearch', false, plugin_basename( dirname( __FILE__ ) ) . '/languages/' );
}

add_action( 'init', 'elastic_instantsearch_load_textdomain' );

require_once ELASTIC_INSTANTSEARCH_PATH . 'classmap.php';


$elastic_instantsearch = Elastic_Instantsearch::get_instance();


if ( defined( 'WP_CLI' ) && WP_CLI ) {
	include ELASTIC_INSTANTSEARCH_PATH . '/includes/class-elastic-instantsearch-cli.php';
	WP_CLI::add_command( 'es', new Elastic_Instantsearch_CLI() );
}
