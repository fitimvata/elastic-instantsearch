import axios from 'axios'
import * as deepmerge from 'deepmerge'

if (!window.elastic_instantsearch) {
  throw new Error('No elastic search config found')
}

const search_client = window.elastic_instantsearch.search_client

function source(params = {}, defaultConfig = {}) {
  let parsedParams ={ ...params }
  let index = ''
  if (parsedParams.body) {
    index = parsedParams.index
    parsedParams = { ...params.body }
  }
  const newParms = deepmerge(defaultConfig || {}, parsedParams, {
    arrayMerge: (_, sourceArray, options) => sourceArray
  })

  return function sourceFn(query, cb) {
    const queryParam = {
      query: {
        query_string: {
          query: query,
        }
      }
    }
    const formatedParams = deepmerge(newParms, queryParam)

    const url = index ? `${search_client}/${index}/_search` : `${search_client}/_search`
    axios.post(url, formatedParams)
      .then(response => {
        const data = response.data
        cb(data.hits.hits, data);
      })
      .catch(error => {
        throw new Error(error.message)
      })
  }
}

export default source