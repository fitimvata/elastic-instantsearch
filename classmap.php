<?php
/**
 * Load all classes
 *
 * @package Elastic_Instantsearch
 */

if ( ! defined( 'ELASTIC_INSTANTSEARCH_PATH' ) ) {
	exit();
}

require __DIR__ . '/vendor/autoload.php';


require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/class-elastic-instantsearch-api.php';
require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/class-elastic-instantsearch-autocomplete-config.php';
require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/class-elastic-instantsearch-compatibility.php';
require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/class-elastic-instantsearch-search.php';
require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/class-elastic-instantsearch-settings.php';
require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/class-elastic-instantsearch-template-loader.php';
require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/class-elastic-instantsearch-utils.php';
require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/class-elastic-instantsearch.php';

require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/indices/class-elastic-instantsearch-index.php';
require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/indices/class-elastic-instantsearch-posts-index.php';
require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/indices/class-elastic-instantsearch-searchable-posts-index.php';
require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/indices/class-elastic-instantsearch-terms-index.php';
require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/indices/class-elastic-instantsearch-users-index.php';

require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/watchers/class-elastic-instantsearch-changes-watcher-interface.php';
require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/watchers/class-elastic-instantsearch-post-changes-watcher.php';
require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/watchers/class-elastic-instantsearch-term-changes-watcher.php';
require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/watchers/class-elastic-instantsearch-user-changes-watcher.php';



if ( is_admin() ) {
	require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/admin/class-elastic-instantsearch-admin.php';
	require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/admin/class-elastic-instantsearch-admin-page-autocomplete.php';
	require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/admin/class-elastic-instantsearch-admin-page-native-search.php';
	require_once ELASTIC_INSTANTSEARCH_PATH . 'includes/admin/class-elastic-instantsearch-admin-page-settings.php';
}
