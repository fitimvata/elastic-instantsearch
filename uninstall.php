<?php
/**
 * Uninstall plugin
 *
 * @package         Elastic_Instantsearch
 */

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	// If uninstall not called from WordPress, then exit.
	exit;
}
