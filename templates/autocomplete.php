<?php
/**
 * Autocomplete templates
 *
 * @package Elastic_Instantsearch/Templates
 */

?>
<script type="text/html" id="tmpl-autocomplete-header">
    <div class="autocomplete-header">
        <div class="autocomplete-header-title">{{{ data.label }}}</div>
        <div class="clear"></div>
    </div>
</script>

<script type="text/html" id="tmpl-autocomplete-post-suggestion">
    <a class="suggestion-link" href="{{ data._source.permalink }}" title="{{ data._source.post_title }}">
        <# if (data._source.images && data._source.images.thumbnail ) { #>
            <img class="suggestion-post-thumbnail" src="{{ data._source.images.thumbnail.url }}" alt="{{ data._source.post_title }}">
        <# } #>
        <div class="suggestion-post-attributes">
            <# if ( data.highlight && data.highlight.post_title && data.highlight.post_title[0] ) { #>
                <span class="suggestion-post-title">{{{ data.highlight.post_title[0] }}}</span>
            <# } else { #>
                <span class="suggestion-post-title">{{{ data._source.post_title }}}</span>
            <# } #>
            <# if (  data.highlight.post_content && data.highlight.post_content[0] ) { #>
                <span class="suggestion-post-content">{{{ data.highlight.post_content[0] }}}</span>
            <# } #>
        </div>
    </a>
</script>

<script type="text/html" id="tmpl-autocomplete-term-suggestion">
    <a class="suggestion-link" href="{{ data._source.permalink }}" title="{{ data._source.name }}">
        <svg viewBox="0 0 21 21" width="21" height="21">
            <svg width="21" height="21" viewBox="0 0 21 21">
                <path
                    d="M4.662 8.72l-1.23 1.23c-.682.682-.68 1.792.004 2.477l5.135 5.135c.7.693 1.8.688 2.48.005l1.23-1.23 5.35-5.346c.31-.31.54-.92.51-1.36l-.32-4.29c-.09-1.09-1.05-2.06-2.15-2.14l-4.3-.33c-.43-.03-1.05.2-1.36.51l-.79.8-2.27 2.28-2.28 2.27zm9.826-.98c.69 0 1.25-.56 1.25-1.25s-.56-1.25-1.25-1.25-1.25.56-1.25 1.25.56 1.25 1.25 1.25z"
                    fill-rule="evenodd"></path>
            </svg>
        </svg>
        <# if ( data.highlight && data.highlight.name && data.highlight.name[0] ) { #>
            <span class="suggestion-post-title">{{{ data.highlight.name[0] }}}</span>
        <# } else { #>
            <span class="suggestion-post-title">{{{ data._source.name }}}</span>
        <# } #>
    </a>
</script>

<script type="text/html" id="tmpl-autocomplete-user-suggestion">
    <a class="suggestion-link user-suggestion-link" href="{{ data._source.posts_url }}" title="{{ data._source.display_name }}">
        <# if ( data._source.avatar_url ) { #>
            <img class="suggestion-user-thumbnail" src="{{ data._source.avatar_url }}" alt="{{ data._source.display_name }}">
        <# } #>
        <# if ( data.highlight && data.highlight.display_name && data.highlight.display_name[0] ) { #>
            <span class="suggestion-post-title">{{{ data.highlight.display_name[0] }}}</span>
        <# } else { #>
            <span class="suggestion-post-title">{{{ data._source.display_name }}}</span>
        <# } #>
    </a>
</script>

<script type="text/html" id="tmpl-autocomplete-footer">
    <div class="autocomplete-footer">
    </div>
</script>

<script type="text/html" id="tmpl-autocomplete-empty">
    <div class="autocomplete-empty">
        <?php esc_html_e( 'No results matched your query ', 'elastic-instantsearch' ); ?>
        <span class="empty-query">"{{ data.query }}"</span>
    </div>
</script>

<script>
(function ($) {
    var sources = [];
    $.each(elastic_instantsearch.autocomplete.sources, function (i, config) {
        var suggestion_template = wp.template(config['tmpl_suggestion']);
        sources.push({
            source: elasticSource({
                index: config['index_name'],
                body: {
                    size: config['max_suggestions'],
                }
            }, config.elastic_config),
            templates: {
                header: function () {
                return wp.template('autocomplete-header')({
                    label: _.escape(config['label'])
                });
                },
                suggestion: function (hit) {

                return suggestion_template(hit);
                }
            }
        });
    });

    $(elastic_instantsearch.autocomplete.input_selector).each(function (i) {
        var $searchInput = $(this);

        var config = {
            debug: elastic_instantsearch.debug,
            hint: false,
            openOnFocus: true,
            appendTo: 'body',
            templates: {
            empty: wp.template('autocomplete-empty')
            }
        };

        /* Instantiate autocomplete.js */
        var autocomplete = window.autocomplete($searchInput[0], config, sources)
        .on('autocomplete:selected', function (e, suggestion) {
            /* Redirect the user when we detect a suggestion selection. */
            window.location.href = suggestion._source.permalink;
        });

        /* Force the dropdown to be re-drawn on scroll to handle fixed containers. */
        jQuery(window).scroll(function() {
            if(autocomplete.autocomplete.getWrapper().style.display === "block") {
            autocomplete.autocomplete.close();
            autocomplete.autocomplete.open();
            }
        });
    });

})(jQuery)
</script>
