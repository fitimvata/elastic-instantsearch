<?php
/**
 * Store all settings
 *
 * @package Elastic_Instantsearch
 */

/**
 * Elastic Instantsearch Settings
 *
 * @class Elastic_Instantsearch_Settings
 */
class Elastic_Instantsearch_Settings {

	/**
	 * Elastic_Instantsearch_Settings constructor.
	 */
	public function __construct() {
        add_option( 'es_client_host', '' );
        add_option( 'es_client_port', '' );
        add_option( 'es_client_scheme', 'http' );
        add_option( 'es_client_user', '' );
        add_option( 'es_client_pass', '' );

        add_option( 'es_search_client_url', '' );

		add_option( 'es_synced_indices_ids', array() );
		add_option( 'es_autocomplete_enabled', 'no' );
		add_option( 'es_autocomplete_config', array() );
		add_option( 'es_override_native_search', 'native' );
		add_option( 'es_index_name_prefix', 'wp_' );
		add_option( 'es_api_is_reachable', 'no' );
	}

	/**
     * Get application host
     *
	 * @return string
	 */
	public function get_client_host() {
		if ( ! $this->is_client_host_in_config() ) {

			return (string) get_option( 'es_client_host', '' );
		}

		$this->assert_constant_is_non_empty_string( ES_CLIENT_HOST, 'ES_CLIENT_HOST' );

		return ES_CLIENT_HOST;
	}

	/**
     * Get search client url
     *
	 * @return string
	 */
	public function get_search_client_url() {
		if ( ! $this->is_search_client_url_in_config() ) {

			return (string) get_option( 'es_search_client_url', '' );
		}

		$this->assert_constant_is_non_empty_string( ES_SEARCH_CLIENT_URL, 'ES_SEARCH_CLIENT_URL' );

		return ES_SEARCH_CLIENT_URL;
	}

	/**
     * Get client port
     *
	 * @return string
	 */
	public function get_client_port() {
		if ( ! $this->is_client_port_in_config() ) {

			return (string) get_option( 'es_client_port', '' );
		}

		$this->assert_constant_is_non_empty_string( ES_CLIENT_PORT, 'ES_CLIENT_PORT' );

		return ES_CLIENT_PORT;
    }

    /**
     * Get client scheme
     *
	 * @return string
	 */
	public function get_client_scheme() {
		if ( ! $this->is_client_scheme_in_config() ) {

			return (string) get_option( 'es_client_scheme', '' );
		}

		$this->assert_constant_is_non_empty_string( ES_CLIENT_SCHEME, 'ES_CLIENT_SCHEME' );

		return ES_CLIENT_SCHEME;
    }

    /**
     * Get client user
     *
	 * @return string
	 */
	public function get_client_user() {
		if ( ! $this->is_client_user_in_config() ) {

			return (string) get_option( 'es_client_user', '' );
		}

		$this->assert_constant_is_non_empty_string( ES_CLIENT_USER, 'ES_CLIENT_USER' );

		return ES_CLIENT_USER;
    }

    /**
     * Get client pass
     *
	 * @return string
	 */
	public function get_client_pass() {
		if ( ! $this->is_client_pass_in_config() ) {

			return (string) get_option( 'es_client_pass', '' );
		}

		$this->assert_constant_is_non_empty_string( ES_CLIENT_PASS, 'ES_CLIENT_PASS' );

		return ES_CLIENT_PASS;
	}

	/**
     * Get blacklist postypes
     *
	 * @return array
	 */
	public function get_post_types_blacklist() {
		$blacklist = (array) apply_filters( 'es_post_types_blacklist', array( 'nav_menu_item' ) );

		// Native WordPress.
		$blacklist[] = 'revision';

		// Native to WordPress VIP platform.
		$blacklist[] = 'kr_request_token';
		$blacklist[] = 'kr_access_token';
		$blacklist[] = 'deprecated_log';
		$blacklist[] = 'async-scan-result';
		$blacklist[] = 'scanresult';

		return array_unique( $blacklist );
	}

	/**
     * Get ids of synced indices
     *
	 * @return array
	 */
	public function get_synced_indices_ids() {
		$ids = array();

		// Gather indices used in autocomplete experience.
		$config = $this->get_autocomplete_config();
		foreach ( $config as $index ) {
			if ( isset( $index['index_id'] ) ) {
				$ids[] = $index['index_id'];
			}
		}

		// Push index used in instantsearch experience.
		// Todo: we should allow users to index without using the shipped search UI or backend implementation.
		if ( $this->should_override_search_in_backend() || $this->should_override_search_with_instantsearch() ) {
			$ids[] = $this->get_native_search_index_id();
		}

		return (array) apply_filters( 'es_get_synced_indices_ids', $ids );
	}


	/**
     * Get blacklisted taxonomies
     *
	 * @return array
	 */
	public function get_taxonomies_blacklist() {
		return (array) apply_filters( 'es_taxonomies_blacklist', array( 'nav_menu', 'link_category', 'post_format' ) );
	}

	/**
     * Get autocomplete enabled
     *
	 * @return string Can be 'yes' or 'no'.
	 */
	public function get_autocomplete_enabled() {
		return get_option( 'es_autocomplete_enabled', 'no' );
	}

	/**
     * Get autocomplete config
     *
	 * @return array
	 */
	public function get_autocomplete_config() {
		return (array) get_option( 'es_autocomplete_config', array() );
	}

	/**
     * Get override native search
     *
	 * @return string Can be 'native' 'backend' or 'instantsearch'.
	 */
	public function get_override_native_search() {
		$search_type = get_option( 'es_override_native_search', 'native' );

		// BC compatibility.
		if ( 'yes' === $search_type ) {
			$search_type = 'backend';
		}

		return $search_type;
	}

	/**
     * Should override search
     *
	 * @return bool
	 */
	public function should_override_search_in_backend() {
		return $this->get_override_native_search() === 'backend' || $this->should_override_search_with_instantsearch();
	}

	/**
     * Should override with instantsearch
     *
	 * @return bool
	 */
	public function should_override_search_with_instantsearch() {
        return false;
        // phpcs:disable
		// $value = $this->get_override_native_search() === 'instantsearch';

        // return (bool) apply_filters( 'es_should_override_search_with_instantsearch', $value );
        // phpcs:enable
	}

	/**
     * Get native search index id
     *
	 * @return string
	 */
	public function get_native_search_index_id() {
		return (string) apply_filters( 'es_native_search_index_id', 'searchable_posts' );
	}

	/**
     * Index name prefix
     *
	 * @return string
	 */
	public function get_index_name_prefix() {
		if ( ! $this->is_index_name_prefix_in_config() ) {

			return (string) get_option( 'es_index_name_prefix', 'wp_' );
		}

		$this->assert_constant_is_non_empty_string( ES_INDEX_NAME_PREFIX, 'ES_INDEX_NAME_PREFIX' );

		return ES_INDEX_NAME_PREFIX;
	}

	/**
	 * Makes sure that constants are non empty strings.
	 * This makes sure that we fail early if the environment configuration is wrong.
	 *
	 * @param mixed $value Value.
	 * @param mixed $constant_name Name.
     * @throws RuntimeException RuntimeException.
	 */
	protected function assert_constant_is_non_empty_string( $value, $constant_name ) {
		if ( ! is_string( $value ) ) {
			throw new RuntimeException( sprintf( 'Constant %s in wp-config.php should be a string, %s given.', $constant_name, gettype( $value ) ) );
		}

		if ( 0 === mb_strlen( $value ) ) {
			throw new RuntimeException( sprintf( 'Constant %s in wp-config.php cannot be empty.', $constant_name ) );
		}
	}

	/**
     * Check ES_CLIENT_HOST
     *
	 * @return bool
	 */
	public function is_client_host_in_config() {
		return defined( 'ES_CLIENT_HOST' );
	}

	/**
     * Check ES_SEARCH_CLIENT_URL
     *
	 * @return bool
	 */
	public function is_search_client_url_in_config() {
		return defined( 'ES_SEARCH_CLIENT_URL' );
	}

	/**
     * Check ES_CLIENT_PORT
     *
	 * @return bool
	 */
	public function is_client_port_in_config() {
		return defined( 'ES_CLIENT_PORT' );
    }

    /**
     * Check ES_CLIENT_SCHEME
     *
	 * @return bool
	 */
    public function is_client_scheme_in_config() {
        return defined( 'ES_CLIENT_SCHEME' );
    }

    /**
     * Check ES_CLIENT_PASS
     *
     * @return bool
     */
    public function is_client_pass_in_config() {
        return defined( 'ES_CLIENT_PASS' );

    }

    /**
     * Check ES_CLIENT_USER
     *
	 * @return bool
	 */
    public function is_client_user_in_config() {
        return defined( 'ES_CLIENT_USER' );
    }

	/**
     * Check ES_INDEX_NAME_PREFIX
     *
	 * @return bool
	 */
	public function is_index_name_prefix_in_config() {
		return defined( 'ES_INDEX_NAME_PREFIX' );
	}

	/**
     * Get api reachability
     *
	 * @return bool
	 */
	public function get_api_is_reachable() {
		$enabled = get_option( 'es_api_is_reachable', 'no' );
		return 'yes' === $enabled;
	}

	/**
     * Set flag for client if it is reachable
     *
	 * @param bool $flag Flag.
	 */
	public function set_api_is_reachable( $flag ) {
		$value = (bool) true === $flag ? 'yes' : 'no';
		update_option( 'es_api_is_reachable', $value );
	}

}
