<?php
/**
 * Elastic search index
 *
 * @package Elastic_Instantsearch\Indices
 */

/**
 * Searchable Posts class
 *
 * @class Elastic_Instantsearch_Searchable_Posts_Index
 */
final class Elastic_Instantsearch_Searchable_Posts_Index extends Elastic_Instantsearch_Index {

	/**
     * Contains only
     *
	 * @var string
	 */
	protected $contains_only = 'posts';

	/**
     * Post types
     *
	 * @var array
	 */
	private $post_types;

	/**
     * Init Elastic_Instantsearch_Searchable_Posts_Index
     *
	 * @param array $post_types Post types.
	 */
	public function __construct( array $post_types ) {
		$this->post_types = $post_types;
	}

	/**
     * Check support
     *
	 * @param mixed $item Item.
	 *
	 * @return bool
	 */
	public function supports( $item ) {
		return $item instanceof WP_Post && in_array( $item->post_type, $this->post_types, true );
	}

	/**
     * Admin panel display name
     *
	 * @return string
	 */
	public function get_admin_name() {
		return __( 'All posts', 'elastic-instantsearch' );
	}

	/**
     * Check if item need to be indexed
     *
	 * @param mixed $item Item.
	 *
	 * @return bool
	 */
	protected function should_index( $item ) {
		return $this->should_index_post( $item );
	}

	/**
     * Check if post need to be indexed
     *
	 * @param WP_Post $post Post.
	 *
	 * @return bool
	 */
	private function should_index_post( WP_Post $post ) {
		$should_index = 'publish' === $post->post_status && empty( $post->post_password );

		return (bool) apply_filters( 'es_should_index_searchable_post', $should_index, $post );
	}

	/**
     * Get record to index
     *
	 * @param mixed $item Otem.
	 *
	 * @return array
	 */
	protected function get_record( $item ) {
        return $this->get_post_record( $item );
    }

	/**
	 * Turns a WP_Post in a collection of records to be pushed to Elastic.
	 *
	 * @param WP_Post $post Post.
	 *
	 * @return array
	 */
	private function get_post_record( WP_Post $post ) {
		$record = $this->get_post_shared_attributes( $post );

		$removed = remove_filter( 'the_content', 'wptexturize', 10 );

		$post_content = apply_filters( 'es_searchable_post_content', $post->post_content, $post );
		$post_content = apply_filters( 'the_content', $post_content );

		if ( true === $removed ) {
			add_filter( 'the_content', 'wptexturize', 10 );
		}

        $record['post_content'] = Elastic_Instantsearch_Utils::prepare_content( $post_content );

		$record = (array) apply_filters( 'es_searchable_post_record', $record, $post );
		$record = (array) apply_filters( 'es_searchable_post_' . $post->post_type . '_record', $record, $post );

		return $record;
	}

	/**
     * Get Post fromated attr
     *
	 * @param WP_Post $post Post.
	 *
	 * @return array
     * @throws RuntimeException Exp.
	 */
	private function get_post_shared_attributes( WP_Post $post ) {
		$shared_attributes              = array();
		$shared_attributes['id']        = $post->ID;
		$shared_attributes['post_id']   = $post->ID;
		$shared_attributes['post_type'] = $post->post_type;

		$post_type = get_post_type_object( $post->post_type );
		if ( null === $post_type ) {
			throw new RuntimeException( 'Unable to fetch the post type information.' );
		}
		$shared_attributes['post_type_label']     = $post_type->labels->name;
		$shared_attributes['post_title']          = $post->post_title;
		$shared_attributes['post_excerpt']        = apply_filters( 'the_excerpt', $post->post_excerpt );
		$shared_attributes['post_date']           = get_post_time( 'U', false, $post );
		$shared_attributes['post_date_formatted'] = get_the_date( '', $post );
		$shared_attributes['post_modified']       = get_post_modified_time( 'U', false, $post );
		$shared_attributes['comment_count']       = (int) $post->comment_count;
		$shared_attributes['menu_order']          = (int) $post->menu_order;

		$author = get_userdata( $post->post_author );
		if ( $author ) {
			$shared_attributes['post_author'] = array(
				'user_id'      => (int) $post->post_author,
				'display_name' => $author->display_name,
				'user_url'     => $author->user_url,
				'user_login'   => $author->user_login,
			);
		}

		$shared_attributes['images'] = Elastic_Instantsearch_Utils::get_post_images( $post->ID );

		$shared_attributes['permalink']      = get_permalink( $post );
		$shared_attributes['post_mime_type'] = $post->post_mime_type;

		// Push all taxonomies by default, including custom ones.
		$taxonomy_objects = get_object_taxonomies( $post->post_type, 'objects' );

		$shared_attributes['taxonomies']              = array();
		$shared_attributes['taxonomies_hierarchical'] = array();
		foreach ( $taxonomy_objects as $taxonomy ) {
			$terms = wp_get_object_terms( $post->ID, $taxonomy->name );
			$terms = is_array( $terms ) ? $terms : array();

			if ( $taxonomy->hierarchical ) {
				$hierarchical_taxonomy_values = Elastic_Instantsearch_Utils::get_taxonomy_tree( $terms, $taxonomy->name );
				if ( ! empty( $hierarchical_taxonomy_values ) ) {
					$shared_attributes['taxonomies_hierarchical'][ $taxonomy->name ] = $hierarchical_taxonomy_values;
				}
			}

			$taxonomy_values = wp_list_pluck( $terms, 'name' );
			if ( ! empty( $taxonomy_values ) ) {
				$shared_attributes['taxonomies'][ $taxonomy->name ] = $taxonomy_values;
			}
		}

		$shared_attributes['is_sticky'] = is_sticky( $post->ID ) ? 1 : 0;

		$shared_attributes = (array) apply_filters( 'es_searchable_post_shared_attributes', $shared_attributes, $post );
		$shared_attributes = (array) apply_filters( 'es_searchable_post_' . $post->post_type . '_shared_attributes', $shared_attributes, $post );

		return $shared_attributes;
	}

	/**
     * Get index settings
     *
	 * @return array
	 */
	protected function get_settings() {
        return (array) apply_filters( 'es_searchable_posts_index_settings', array() );
	}

	/**
     * Get index mappings
     *
	 * @return array
	 */
	protected function get_mappings() {
        return (array) apply_filters( 'es_searchable_posts_index_mappings', array() );
    }

    /**
     * Get default autocomplete config
     *
     * @return array
     */
    public function get_default_autocomplete_config() {
        $config                   = parent::get_default_autocomplete_config();
        $config['elastic_config'] = [
            'query'     => [
                'query_string' => [
                    'fields' => [ 'post_title', 'post_content' ],
                ],
            ],
            'highlight' => [
                'fields' => [
                    'post_title'   => [
                        'number_of_fragments' => 0,
                    ],
                    'post_content' => [
                        'number_of_fragments' => 1,
                    ],
                ],
            ],
        ];
		$config                   = apply_filters( 'es_searchable_posts_index_autocomplete_config', $config );
        return $config;
    }

	/**
     * Get Post id
     *
	 * @param WP_Post $item Post.
	 *
	 * @return string
	 */
	public function get_item_id( $item ) {
		return $item->ID;
	}

	/**
     * Update record
     *
	 * @param mixed $item Item.
	 * @param array $record Record.
	 */
	protected function update_record( $item, array $record ) {
		$this->update_post_record( $item, $record );
	}

	/**
     * Update record
     *
	 * @param WP_Post $post Post.
	 * @param array   $record Record.
	 */
	private function update_post_record( WP_Post $post, array $record ) {
		// If there are no records, parent `update_records` will take care of the deletion.
		// In case of posts, we ALWAYS need to delete existing records.
		if ( ! empty( $record ) ) {
			$this->delete_item( $post );
		}

		parent::update_record( $post, $record );

		do_action( 'es_searchable_posts_index_post_updated', $post, $record );
		do_action( 'es_searchable_posts_index_post_' . $post->post_type . '_updated', $post, $record );
	}

	/**
     * Get index id
     *
	 * @return string
	 */
	public function get_id() {
		return 'searchable_posts';
	}

	/**
     * Get index item count
     *
	 * @return int
	 */
	protected function get_re_index_items_count() {
		$query = new WP_Query(
			array(
				'post_type'              => $this->post_types,
				'post_status'            => 'any', // Let the `should_index` take care of the filtering.
				'suppress_filters'       => true,
				'cache_results'          => false,
				'lazy_load_term_meta'    => false,
				'update_post_term_cache' => false,
			)
		);

		return (int) $query->found_posts;
	}

	/**
     * Get items
     *
	 * @param int $page Page number.
	 * @param int $batch_size Page size.
	 *
	 * @return array
	 */
	protected function get_items( $page, $batch_size ) {
		$query = new WP_Query(
			array(
				'post_type'              => $this->post_types,
				'posts_per_page'         => $batch_size,
				'post_status'            => 'any',
				'order'                  => 'ASC',
				'orderby'                => 'ID',
				'paged'                  => $page,
				'suppress_filters'       => true,
				'cache_results'          => false,
				'lazy_load_term_meta'    => false,
				'update_post_term_cache' => false,
			)
		);

		return $query->posts;
	}
}
