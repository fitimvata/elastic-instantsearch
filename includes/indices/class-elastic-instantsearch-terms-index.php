<?php
/**
 * Elastic search index
 *
 * @package Elastic_Instantsearch\Indices
 */

/**
 * Terms class
 *
 * @class Elastic_Instantsearch_Terms_Index
 */
final class Elastic_Instantsearch_Terms_Index extends Elastic_Instantsearch_Index {

	/**
     * Contains only
     *
	 * @var string
	 */
	protected $contains_only = 'terms';

	/**
     * Taxonomy
     *
	 * @var string
	 */
	private $taxonomy;

	/**
	 * Elastic_Instantsearch_Terms_Index constructor.
	 *
	 * @param string $taxonomy Taxonomy.
	 */
	public function __construct( $taxonomy ) {
		$this->taxonomy = (string) $taxonomy;
	}

	/**
     * Get admin label
     *
	 * @return string The name displayed in the admin UI.
	 */
	public function get_admin_name() {
		$taxonomy = get_taxonomy( $this->taxonomy );

		return $taxonomy->labels->name;
	}

	/**
     * Should index
     *
	 * @param mixed $item Item.
	 * @return bool
	 */
	protected function should_index( $item ) {
		// For now we index the term if it is in use somewhere.
		$should_index = $item->count > 0;

		return (bool) apply_filters( 'es_should_index_term', $should_index, $item );
	}

	/**
     * Get record
     *
	 * @param mixed $item Item.
	 * @return array
	 */
	protected function get_record( $item ) {
		$record                = array();
		$record['id']          = $item->term_id;
		$record['term_id']     = $item->term_id;
		$record['taxonomy']    = $item->taxonomy;
		$record['name']        = html_entity_decode( $item->name );
		$record['description'] = $item->description;
		$record['slug']        = $item->slug;
		$record['posts_count'] = (int) $item->count;
		if ( function_exists( 'wpcom_vip_get_term_link' ) ) {
			$record['permalink'] = wpcom_vip_get_term_link( $item );
		} else {
			$record['permalink'] = get_term_link( $item );
		}

		$record = (array) apply_filters( 'es_term_record', $record, $item );
		$record = (array) apply_filters( 'es_term_' . $item->taxonomy . '_record', $record, $item );

		return $record;
	}

	/**
     * Get reindex item counts
     *
	 * @return int
	 */
	protected function get_re_index_items_count() {
		return (int) wp_count_terms( $this->taxonomy );
	}

	/**
     * Get index settings
     *
	 * @return array
	 */
	protected function get_settings() {
		$settings = array();
		$settings = (array) apply_filters( 'es_terms_index_settings', $settings, $this->taxonomy );
		$settings = (array) apply_filters( 'es_terms_' . $this->taxonomy . '_index_settings', $settings );
		return $settings;
    }

	/**
     * Get index mappings
     *
	 * @return array
	 */
	protected function get_mappings() {
		return (array) apply_filters( 'es_terms_index_mappings', array() );
    }

    /**
     * Get default config
     */
	public function get_default_autocomplete_config() {
		$config = array(
			'position'        => 20,
			'max_suggestions' => 3,
            'tmpl_suggestion' => 'autocomplete-term-suggestion',
            'elastic_config'  => [
                'query'     => [
                    'query_string' => [
                        'fields' => [ 'name' ],
                    ],
                ],
                'highlight' => [
                    'fields' => [
                        'name' => [
                            'number_of_fragments' => 0,
                        ],
                    ],
                ],
            ],
        );
        $config = array_merge( parent::get_default_autocomplete_config(), $config );
        $config = apply_filters( 'es_terms_index_autocomplete_config', $config, $this->taxonomy );
		$config = apply_filters( 'es_terms_' . $this->taxonomy . '_index_autocomplete_config', $config );
		return $config;
	}

	/**
     * Get id
     *
	 * @return string
	 */
	public function get_id() {
		return 'terms_' . $this->taxonomy;
    }

    /**
     * Get Term id
     *
	 * @param WP_Term $item Post.
	 *
	 * @return string
	 */
	public function get_item_id( $item ) {
		return $item->term_id;
	}


	/**
     * Get items
     *
	 * @param int $page Page number.
	 * @param int $batch_size Batch size.
	 *
	 * @return array
	 */
	protected function get_items( $page, $batch_size ) {
		$offset = $batch_size * ( $page - 1 );

		$args = array(
			'order'      => 'ASC',
			'orderby'    => 'id',
			'offset'     => $offset,
			'number'     => $batch_size,
			'hide_empty' => false, // Let users choose what to index.
		);

		// We use prior to 4.5 syntax for BC purposes.
		return get_terms( $this->taxonomy, $args );
	}

	/**
	 * A performing function that return true if the item can potentially
	 * be subject for indexation or not. This will be used to determine if an item is part of the index
	 * As this function will be called synchronously during other operations,
	 * it has to be as lightweight as possible. No db calls or huge loops.
	 *
	 * @param mixed $item Item.
	 *
	 * @return bool
	 */
	public function supports( $item ) {
		return isset( $item->term_id )
			&& is_int( $item->term_id )
			&& isset( $item->taxonomy )
			&& $item->taxonomy === $this->taxonomy;
	}
}
