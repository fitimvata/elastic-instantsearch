<?php
/**
 * Elastic search index
 *
 * @package Elastic_Instantsearch\Indices
 */

/**
 * Users class
 *
 * @class Elastic_Instantsearch_Terms_Index
 */
final class Elastic_Instantsearch_Users_Index extends Elastic_Instantsearch_Index {

	/**
     * Contains only
     *
	 * @var string
	 */
	protected $contains_only = 'users';

	/**
     * Get admin label
     *
	 * @return string The name displayed in the admin UI.
	 */
	public function get_admin_name() {
		return __( 'Users' );
	}

	/**
     * Should index
     *
	 * @param mixed $item Item.
	 * @return bool
	 */
	protected function should_index( $item ) {
		if ( function_exists( 'wpcom_vip_count_user_posts' ) ) {
			$should_index = (int) wpcom_vip_count_user_posts( $item->ID ) > 0;
		} else {
			$should_index = (int) count_user_posts( $item->ID ) > 0;
		}

		return (bool) apply_filters( 'es_should_index_user', $should_index, $item );
	}

	/**
     * Get record
     *
	 * @param mixed $item Item.
	 * @return array
	 */
	protected function get_record( $item ) {
		$record                 = array();
		$record['objectID']     = $item->ID;
		$record['user_id']      = $item->ID;
		$record['display_name'] = $item->display_name;
		$record['posts_url']    = get_author_posts_url( $item->ID, $item->user_nicename );
		$record['description']  = get_the_author_meta( 'description', $item->ID );

		if ( function_exists( 'wpcom_vip_count_user_posts' ) ) {
			$record['posts_count'] = (int) wpcom_vip_count_user_posts( $item->ID );
		} else {
			$record['posts_count'] = (int) count_user_posts( $item->ID );
		}

		$avatar_size = 32;
		if ( function_exists( 'get_avatar_url' ) ) {
			$record['avatar_url'] = get_avatar_url(
                $item->ID,
                array(
					'size' => $avatar_size,
				)
			);
		} else {
			$email_hash           = md5( strtolower( trim( $item->user_email ) ) );
			$record['avatar_url'] = 'https://www.gravatar.com/avatar/' . $email_hash . '?s=' . $avatar_size;
		}

		$record = (array) apply_filters( 'es_user_record', $record, $item );

		return $record;
	}

	/**
     * Get reindex item counts
     *
	 * @return int
	 */
	protected function get_re_index_items_count() {
		$users_count = count_users();

		return (int) $users_count['total_users'];
	}

	/**
     * Get index settings
     *
	 * @return array
	 */
	protected function get_settings() {
		$settings = array();

		return (array) apply_filters( 'es_users_index_settings', $settings );
    }

    /**
     * Get index mappings
     *
	 * @return array
	 */
	protected function get_mappings() {
		return (array) apply_filters( 'es_users_index_mappings', array() );
    }

	/**
     * Get id
     *
	 * @return string
	 */
	public function get_id() {
		return 'users';
    }

    /**
     * Get User id
     *
	 * @param mixed $item Post.
	 *
	 * @return string
	 */
	public function get_item_id( $item ) {
		return $item->ID;
	}


	/**
     * Get items
     *
	 * @param int $page Page number.
	 * @param int $batch_size Batch size.
	 *
	 * @return array
	 */
	protected function get_items( $page, $batch_size ) {
		$offset = $batch_size * ( $page - 1 );

		$args = array(
			'order'   => 'ASC',
			'orderby' => 'ID',
			'offset'  => $offset,
			'number'  => $batch_size,
		);

		// We use prior to 4.5 syntax for BC purposes, no `paged` arg.
		return get_users( $args );
	}

	/**
	 * A performing function that return true if the item can potentially
	 * be subject for indexation or not. This will be used to determine if an item is part of the index
	 * As this function will be called synchronously during other operations,
	 * it has to be as lightweight as possible. No db calls or huge loops.
	 *
	 * @param mixed $item Item.
	 * @return bool
	 */
	public function supports( $item ) {
		return $item instanceof WP_User;
	}

    /**
     * Get default config
     */
	public function get_default_autocomplete_config() {
		$config = array(
			'position'        => 30,
			'max_suggestions' => 3,
            'tmpl_suggestion' => 'autocomplete-user-suggestion',
            'elastic_config'  => [
                'query'     => [
                    'query_string' => [
                        'fields' => [ 'display_name' ],
                    ],
                ],
                'highlight' => [
                    'fields' => [
                        'display_name' => [
                            'number_of_fragments' => 0,
                        ],
                    ],
                ],
            ],
        );
        $config = array_merge( parent::get_default_autocomplete_config(), $config );
        $config = apply_filters( 'es_users_index_autocomplete_config', $config );
		return $config;
	}
}
