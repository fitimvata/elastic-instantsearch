<?php
/**
 * Elastic search index
 *
 * @package Elastic_Instantsearch\Indices
 */

use Elasticsearch\Client;
use Elasticsearch\Common\Exceptions\Missing404Exception;

/**
 * Elastic search base Index
 *
 * @class Elastic_Instantsearch_Index
 */
abstract class Elastic_Instantsearch_Index {

	/**
     * Elasticsearch client instance
     *
	 * @var Client
	 */
	private $client;

	/**
     * Enabled
     *
	 * @var bool
	 */
	private $enabled = false;

	/**
     * Name Prefix
     *
	 * @var string
	 */
	private $name_prefix = '';

	/**
     * Contains only
     *
	 * @var string|null Should be one of posts, terms or users or left null.
	 */
	protected $contains_only;

	/**
     * Get admin name
     *
	 * @return string The name displayed in the admin UI.
	 */
	abstract public function get_admin_name();

	/**
     * Check if index contain given type
     *
	 * @param string $type Index Type.
	 * @return bool
	 */
	final public function contains_only( $type ) {
		if ( null === $this->contains_only ) {
			return false;
		}

		return $this->contains_only === $type;
	}

	/**
	 * A performing function that return true if the item can potentially
	 * be subject for indexation or not. This will be used to determine if an item is part of the index
	 * As this function will be called synchronously during other operations,
	 * it has to be as lightweight as possible. No db calls or huge loops.
	 *
	 * @param mixed $item Item.
	 *
	 * @return bool
	 */
	abstract public function supports( $item );

    /**
     * Assert is supported
     *
     * @param mixed $item Item.
     * @throws RuntimeException Throws if item is not supported.
     */
	public function assert_is_supported( $item ) {
		if ( ! $this->supports( $item ) ) {
			throw new RuntimeException( 'Item is no supported on this index.' );
		}
	}

	/**
     * Set Elastic client
     *
	 * @param \Elasticsearch\Client $client Elastic client instance.
	 */
	final public function set_client( Client $client ) {
		$this->client = $client;
	}

	/**
     * Get Elastic client
     *
	 * @return \Elasticsearch\Client
     * @throws LogicException Throws when client has not been set.
	 */
	final protected function get_client() {
		if ( null === $this->client ) {
			throw new LogicException( 'Client has not been set.' );
		}

		return $this->client;
	}

	/**
     * Get searched items from elastic search
     *
	 * @param string      $query Query string.
	 * @param array|null  $args All raw args.
     * @param string|null $order_by Order by field.
     * @param string      $order Order direction.
	 *
	 * @return array
	 */
	final public function search( $query, $args = [], $order_by = null, $order = 'desc' ) {
        $order_args = [];
        if ( $order_by ) {
            $order_args[] = [
                $order_by => [ 'order' => $order ],
            ];
        }
        $args = array_replace_recursive(
            [
                'query' => [
                    'query_string' => [
                        'query' => $query,
                    ],
                ],
                'sort'  => array_merge( $args['sort'] ?? [], $order_args ),
            ],
            $args
        );

        return $this->client->search(
            [
                'index' => $this->get_name(),
                'type'  => $this->get_name(),
                'body'  => $args,
            ]
        );
	}

	/**
     * Set enable flag
     *
	 * @param bool $flag Bool flag.
	 */
	final public function set_enabled( $flag ) {
		$this->enabled = (bool) $flag;
	}

	/**
     * Check enable flag
     *
	 * @return bool
	 */
	final public function is_enabled() {
		return $this->enabled;
	}

	/**
     * Set name prefix
     *
	 * @param string $prefix Prefix.
	 */
	final public function set_name_prefix( $prefix ) {
		$this->name_prefix = (string) $prefix;
	}

	/**
     * Sync record
     *
	 * @param mixed $item Item array.
	 */
	public function sync( $item ) {
        $this->create_index_if_not_existing( false );
		$this->assert_is_supported( $item );
		if ( $this->should_index( $item ) ) {
			do_action( 'es_before_get_record', $item );
			$record = $this->get_record( $item );
			do_action( 'es_after_get_record', $item );

			$this->update_record( $item, $record );
			return;
		}

		$this->delete_item( $item );
	}

	/**
     * Check if given iteem should index
     *
	 * @param mixed $item Item array.
	 *
	 * @return bool
	 */
	abstract protected function should_index( $item );

	/**
     * Get Elastic Record
     *
	 * @param mixed $item Item.
	 * @return array
	 */
    abstract protected function get_record( $item );

    /**
     * Get formated records for bulk update
     *
     * @param array $records Formated records.
     * @return array
     */
    protected function get_bulk_records( array $records ) {
        $formated_records = array();

        foreach ( $records as $index => $item ) {
            $formated_records[] = [
                'index' => [
                    '_index' => $this->get_name(),
                    '_type'  => $this->get_name(),
                    '_id'    => $item['id'],
                ],
            ];
            $formated_records[] = $item;
        }

        return $formated_records;
    }

	/**
     * Update Elastic record
     *
	 * @param mixed $item Item.
	 * @param array $record Formated Record.
	 */
	protected function update_record( $item, array $record ) {
		if ( empty( $record ) ) {
			$this->delete_item( $item );
			return;
		}

		$record = $this->sanitize_json_data( $record );
        $this->client->index(
            [
                'index' => $this->get_name(),
                'type'  => $this->get_name(),
                'id'    => $record['id'],
                'body'  => $record,
            ]
        );
	}

	/**
     * Get Index Name
     *
	 * @param string $prefix Prefix.
	 *
	 * @return string
	 */
	public function get_name( $prefix = null ) {
		if ( null === $prefix ) {
			$prefix = $this->name_prefix;
		}

		return $prefix . $this->get_id();
	}

	/**
     * Reindex
     *
	 * @param int $page Page number.
     * @throws InvalidArgumentException Invalid argumnet exeption.
	 */
	public function re_index( $page ) {
		$page = (int) $page;

		if ( $page < 1 ) {
			throw new InvalidArgumentException( 'Page should be superior to 0.' );
		}

		if ( 1 === $page ) {
			$this->create_index_if_not_existing();
        }

		$batch_size = (int) $this->get_re_index_batch_size();
		if ( $batch_size < 1 ) {
			throw new InvalidArgumentException( 'Re-index batch size can not be lower than 1.' );
        }

        $items_count   = $this->get_re_index_items_count();
        $max_num_pages = (int) max( ceil( $items_count / $batch_size ), 1 );
        $items         = $this->get_items( $page, $batch_size );
        $records       = array();

		foreach ( $items as $item ) {
			if ( ! $this->should_index( $item ) ) {
				$this->delete_item( $item );
				continue;
			}
			do_action( 'es_before_get_record', $item );
			$records[] = $this->get_record( $item );
			do_action( 'es_after_get_record', $item );
		}

        $records = $this->get_bulk_records( $records );
		if ( ! empty( $records ) ) {

			$records = $this->sanitize_json_data( $records );
            $this->client->bulk(
                [
                    'body' => $records,
                ]
            );
		}

		if ( $page === $max_num_pages ) {
			do_action( 'es_re_indexed_items', $this->get_id() );
		}
	}

    /**
     * Create index if dont exits
     *
     * @param bool $clear_if_existing Force index deletion.
     */
	public function create_index_if_not_existing( $clear_if_existing = true ) {
        if ( $this->exists() ) {
            if ( false === $clear_if_existing ) {
				return;
            }
            $this->delete();
        }

        $this->push_settings();
	}

    /**
     * Create index
     */
	public function push_settings() {
        $args     = [ 'index' => $this->get_name() ];
        $settings = $this->get_settings();
        if ( ! empty( $settings ) ) {
            $args['body']['settings'] = $settings;
        }

        $mappings = $this->get_mappings();
        if ( ! empty( $mappings ) ) {
            $args['body']['mappings'] = $mappings;
        }

        if ( $this->exists() ) {
            if ( ! empty( $settings ) ) {
                $this->client->indices()->putSettings(
                    [
                        'index' => $this->get_name(),
                        'body'  => [
                            'settings' => $settings,
                        ],
                    ]
                );
            };
            if ( ! empty( $mappings ) ) {
                $body                      = [];
                $body[ $this->get_name() ] = $mappings;
                $this->client->indices()->putMapping(
                    [
                        'index' => $this->get_name(),
                        'type'  => $this->get_name(),
                        'body'  => $body,
                    ]
                );
            }
            return;
        }

        $this->client->indices()->create( $args );
	}

	/**
	 * Sanitize data to allow non UTF-8 content to pass.
	 * Here we use a private function introduced in WP 4.1.
	 *
	 * @param mixed $data Data to sanitize.
	 *
	 * @return mixed
	 * @throws Exception Exception.
	 */
	protected function sanitize_json_data( $data ) {
		if ( function_exists( '_wp_json_sanity_check' ) ) {
			return _wp_json_sanity_check( $data, 512 );
		}

		return $data;
	}

	/**
     * Get index item count
     *
	 * @return int
	 */
	abstract protected function get_re_index_items_count();

	/**
     * Check if is last page
     *
	 * @param int $page Page number.
	 * @return bool
	 */
	protected function is_last_page_to_re_index( $page ) {

		return (int) $page >= $this->get_re_index_max_num_pages();
	}

	/**
     * Get number of pages
     *
	 * @return int
	 */
	public function get_re_index_max_num_pages() {
		$items_count = $this->get_re_index_items_count();

		return (int) ceil( $items_count / $this->get_re_index_batch_size() );
	}

    /**
     * De index all items - Delete index
     */
	public function de_index_items() {
		$this->delete();
		do_action( 'es_de_indexed_items', $this->get_id() );
	}

	/**
     * Reindex batch size
     *
	 * @return int
	 */
	protected function get_re_index_batch_size() {
		$batch_size = (int) apply_filters( 'es_indexing_batch_size', 100 );
		$batch_size = (int) apply_filters( 'es_' . $this->get_id() . '_indexing_batch_size', $batch_size );

		return $batch_size;
	}

	/**
     * Get index settings
     *
	 * @return array
	 */
	abstract protected function get_settings();

    /**
     * Get index mappings
     *
	 * @return array
	 */
	abstract protected function get_mappings();

	/**
     * Get index id
     *
	 * @return string
	 */
    abstract public function get_id();

    /**
     * Return item id
     *
     * @param mixed $item Item.
	 * @return string
	 */
	abstract public function get_item_id( $item );

	/**
     * Get items
     *
	 * @param int $page Page number.
	 * @param int $batch_size Page size.
	 *
	 * @return array
	 */
	abstract protected function get_items( $page, $batch_size );

    /**
     * Get default autocomplete config
     *
     * @return array
     */
	public function get_default_autocomplete_config() {
		return apply_filters(
            'es_autocomplete_config',
            array(
                'index_id'        => $this->get_id(),
                'index_name'      => $this->get_name(),
                'label'           => $this->get_admin_name(),
                'admin_name'      => $this->get_admin_name(),
                'position'        => 10,
                'max_suggestions' => 5,
                'tmpl_suggestion' => 'autocomplete-post-suggestion',
                'elastic_config'  => null,
            ),
            $this->get_id()
        );
	}

	/**
     * Return index array
     *
	 * @return array
	 */
	public function to_array() {
		return array(
			'name'    => $this->get_name(),
			'id'      => $this->get_id(),
			'enabled' => $this->enabled,
		);
	}

	/**
     * Delete Elastic record
     *
	 * @param mixed $item Item.
	 */
	public function delete_item( $item ) {
		$this->assert_is_supported( $item );

        try {
            $this->client->delete(
                [
                    'index' => $this->get_name(),
                    'type'  => $this->get_name(),
                    'id'    => $this->get_item_id( $item ),
                ]
            );
        } catch ( Missing404Exception $exception ) { // phpcs:disable
            // dont exists.
        }
	}

	/**
	 * Returns true if the index exists in Elastic.
	 * false otherwise.
	 *
	 * @return bool
	 */
	public function exists() {
        return $this->client->indices()->exists(
            [
                'index' => $this->get_name(),
            ]
        );
	}

    /**
     * Delete index
     */
	public function delete() {
        $this->client->indices()->delete(
            [
                'index' => $this->get_name(),
            ]
        );
	}

}
