<?php
/**
 * Term changes watcher
 *
 * @package Elastic_Instantsearch/Watchers
 */

/**
 * Class to watch post changes and update to elasticsearch
 *
 * @class Elastic_Instantsearch_Post_Changes_Watcher
 */
class Elastic_Instantsearch_Term_Changes_Watcher implements Elastic_Instantsearch_Changes_Watcher_Interface {

	/**
     * Index
     *
	 * @var Elastic_Instantsearch_Index
	 */
	private $index;

	/**
     * Elastic_Instantsearch_Term_Changes_Watcher init
     *
	 * @param Elastic_Instantsearch_Index $index Index.
	 */
	public function __construct( Elastic_Instantsearch_Index $index ) {
		$this->index = $index;
	}

    /**
     * Watch term
     */
	public function watch() {
		// Fires immediately after the given terms are edited.
		add_action( 'edited_term', array( $this, 'sync_item' ) );

		// Fires after an object's terms have been set.
		add_action( 'set_object_terms', array( $this, 'handle_changes' ), 10, 6 );

		// Fires after a term is deleted from the database and the cache is cleaned.
		add_action( 'delete_term', array( $this, 'on_delete_term' ), 10, 4 );

	}

	/**
     * Sync item
     *
	 * @param int $term_id Term id.
	 */
	public function sync_item( $term_id ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		$term = get_term( (int) $term_id );

		if ( ! $term || ! $this->index->supports( $term ) ) {
			return;
		}

		try {
			$this->index->sync( $term );
		} catch ( Exception $exception ) {
			error_log( $exception->getMessage() ); // phpcs:ignore
		}
	}

	/**
     * Handle changes
     *
	 * @param mixed $object_id Object id.
	 * @param mixed $terms Terms.
	 * @param mixed $tt_ids Tt ids.
	 * @param mixed $taxonomy Taxonomy.
	 * @param mixed $append Append.
	 * @param mixed $old_tt_ids Old tt ids.
	 */
	public function handle_changes( $object_id, $terms, $tt_ids, $taxonomy, $append, $old_tt_ids ) {
		$terms_to_sync = array_unique( array_merge( $terms, $old_tt_ids ) );

		foreach ( $terms_to_sync as $term_id ) {
			$this->sync_item( $term_id );
		}
	}

	/**
     * Handle delete
     *
	 * @param mixed $term Term.
	 * @param mixed $tt_id Tt id.
	 * @param mixed $taxonomy Tax.
	 * @param mixed $deleted_term Deleted term.
	 */
	public function on_delete_term( $term, $tt_id, $taxonomy, $deleted_term ) {
		if ( ! $this->index->supports( $deleted_term ) ) {
			return;
		}

		try {
			$this->index->delete_item( $deleted_term );
		} catch ( Exception $exception ) {
			error_log( $exception->getMessage() ); // phpcs:ignore
		}
	}
}
