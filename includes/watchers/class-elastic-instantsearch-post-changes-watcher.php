<?php
/**
 * Post changes watcher
 *
 * @package Elastic_Instantsearch/Watchers
 */

/**
 * Class to watch post changes and update to elasticsearch
 *
 * @class Elastic_Instantsearch_Post_Changes_Watcher
 */
class Elastic_Instantsearch_Post_Changes_Watcher implements Elastic_Instantsearch_Changes_Watcher_Interface {

	/**
     * Index
     *
	 * @var Elastic_Instantsearch_Index
	 */
	private $index;

	/**
     * Init Watcher
     *
	 * @param Elastic_Instantsearch_Index $index Index.
	 */
	public function __construct( Elastic_Instantsearch_Index $index ) {
		$this->index = $index;
	}

    /**
     * Watch post changes
     */
	public function watch() {
		// Fires once a post has been saved.
		add_action( 'save_post', array( $this, 'sync_item' ) );

		// Fires before a post is deleted, at the start of wp_delete_post().
		// At this stage the post metas are still available, and we need them.
		add_action( 'before_delete_post', array( $this, 'delete_item' ) );

		// Handle meta changes after the change occurred.
		add_action( 'added_post_meta', array( $this, 'on_meta_change' ), 10, 4 );
		add_action( 'updated_post_meta', array( $this, 'on_meta_change' ), 10, 4 );
		add_action( 'deleted_post_meta', array( $this, 'on_meta_change' ), 10, 4 );

		// Handle attachment changes. These are required because the other post hooks are not triggered.
		add_action( 'add_attachment', array( $this, 'sync_item' ) );
		add_action( 'attachment_updated', array( $this, 'sync_item' ) );
		add_action( 'delete_attachment', array( $this, 'delete_item' ) );
	}

	/**
     * Sync post
     *
	 * @param int $post_id Post id.
	 */
	public function sync_item( $post_id ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		$post = get_post( (int) $post_id );
		if ( ! $post || ! $this->index->supports( $post ) ) {
			return;
        }

		try {
            $this->index->sync( $post );
		} catch ( Exception $exception ) {
			error_log( $exception->getMessage() ); // phpcs:ignore
		}
	}

	/**
     * Delete post
     *
	 * @param int $post_id Post id.
	 */
	public function delete_item( $post_id ) {
		$post = get_post( (int) $post_id );
		if ( ! $post || ! $this->index->supports( $post ) ) {
			return;
		}

		try {
			$this->index->delete_item( $post );
		} catch ( Exception $exception ) {
			error_log( $exception->getMessage() ); // phpcs:ignore
		}
	}

	/**
     * Sync post on meta change
     *
	 * @param string|array $meta_id Meta id.
	 * @param int          $object_id Object id.
	 * @param string       $meta_key Meta key.
	 */
	public function on_meta_change( $meta_id, $object_id, $meta_key ) {
		$keys = array( '_thumbnail_id' );
		$keys = (array) apply_filters( 'es_watch_post_meta_keys', $keys, $object_id );

		if ( ! in_array( $meta_key, $keys ) ) { // phpcs:ignore
			return;
		}

		$this->sync_item( $object_id );
	}
}
