<?php
/**
 * Changes Watcher Interface
 *
 * @package Elastic_Instantsearch/Watchers
 */

/**
 * Elastic Instantsearch Changes Watcher Interface
 *
 * Functions that must be defined by order store classes.
 */
interface Elastic_Instantsearch_Changes_Watcher_Interface {

    /**
     * Watch changes and do something with it
     */
	public function watch();
}
