<?php
/**
 * Elastic api
 *
 * @package Elastic_Instantsearch
 */

use Elasticsearch\ClientBuilder;
use Elasticsearch\Client;

/**
 * Elastic Instantsearch API
 *
 * @class Elastic_Instantsearch_API
 */
class Elastic_Instantsearch_API {

	/**
     * Client
     *
	 * @var Elasticsearch\Client
	 */
	private $client;

	/**
     * Settings.
     *
	 * @var Elastic_Instantsearch_Settings
	 */
	private $settings;

	/**
     * Elastic_Instantsearch_API init
     *
	 * @param Elastic_Instantsearch_Settings $settings Settings.
	 */
	public function __construct( Elastic_Instantsearch_Settings $settings ) {
		$this->settings = $settings;
	}

    /**
     * Check reachability
     */
	public function is_reachable() {
		if ( ! $this->settings->get_api_is_reachable() ) {
			return false;
		}

        $client = $this->get_client();

		return null !== $client;
	}

	/**
     * Get elastic client
     *
	 * @return Elasticsearch\Client|null
	 */
	public function get_client() {
		$client_host       = $this->settings->get_client_host();
		$client_port       = $this->settings->get_client_port();
		$client_scheme     = $this->settings->get_client_scheme();
		$client_user       = $this->settings->get_client_user();
		$client_pass       = $this->settings->get_client_pass();
		$search_client_url = $this->settings->get_search_client_url();

		if ( empty( $client_host ) || empty( $search_client_url ) ) {
			return null;
		}

		if ( null === $this->client ) {
			$this->client = ClientBuilder::create()->setHosts(
                [
                    [
                        'host'   => $client_host,
                        'scheme' => $client_scheme,
                        'port'   => $client_port,
                        'user'   => $client_user,
                        'pass'   => $client_pass,
                    ],
                ]
            )->build();
		}
		return $this->client;
    }

    /**
     * Assert Valid Credentials
     *
	 * @param string $client_host Host.
	 * @param string $client_port Port.
	 * @param string $client_scheme Scheme.
	 * @param string $client_user User.
	 * @param string $client_pass Pass.
	 *
     * @return array
	 * @throws Exception Exp.
	 */
	public static function assert_valid_credentials( $client_host, $client_port, $client_scheme, $client_user, $client_pass ) {
		$client = ClientBuilder::create()->setHosts(
            [
                [
                    'host'   => $client_host,
                    'scheme' => $client_scheme,
                    'port'   => $client_port,
                    'user'   => $client_user,
                    'pass'   => $client_pass,
                ],
            ]
        )->build();

		// This checks if the API Key is an Admin API key.
		// Admin API keys have no scopes so we need a separate check here.
        return $client->info();
	}

}
