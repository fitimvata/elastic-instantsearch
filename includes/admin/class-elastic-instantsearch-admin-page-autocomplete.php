<?php
/**
 * Autocomplete settings
 *
 * @package Elastic_Instantsearch/Admin
 */

/**
 * Elastic_Instantsearch_Admin_Page_Autocomplete
 *
 * @class Elastic_Instantsearch_Admin_Page_Autocomplete
 */
class Elastic_Instantsearch_Admin_Page_Autocomplete {

	/**
     * Slug.
     *
	 * @var string
	 */
	private $slug = 'elastic-instantsearch';

	/**
     * Capability
     *
	 * @var string
	 */
	private $capability = 'manage_options';

	/**
     * Section
     *
	 * @var string
	 */
	private $section = 'es_section_autocomplete';

	/**
     * Option group
     *
	 * @var string
	 */
	private $option_group = 'es_autocomplete';

	/**
     * Settings
     *
	 * @var Elastic_Instantsearch_Settings
	 */
	private $settings;

	/**
     * Autocomplete config.
     *
	 * @var Elastic_Instantsearch_Autocomplete_Config
	 */
	private $autocomplete_config;

	/**
     * Init
     *
	 * @param Elastic_Instantsearch_Settings            $settings Settings.
	 * @param Elastic_Instantsearch_Autocomplete_Config $autocomplete_config Autocomplete config.
	 */
	public function __construct( Elastic_Instantsearch_Settings $settings, Elastic_Instantsearch_Autocomplete_Config $autocomplete_config ) {
		$this->settings            = $settings;
		$this->autocomplete_config = $autocomplete_config;

		add_action( 'admin_menu', array( $this, 'add_page' ) );
		add_action( 'admin_init', array( $this, 'add_settings' ) );
		add_action( 'admin_notices', array( $this, 'display_errors' ) );

		// todo: listen for de-index to remove from autocomplete.
	}

    /**
     * Add page.
     */
	public function add_page() {
		add_menu_page(
			'Elastic Search',
			esc_html__( 'Elastic Search', 'elastic-instantsearch' ),
			'manage_options',
			'elastic-instantsearch',
            array( $this, 'display_page' ),
            ELASTIC_LOGO_WHITE
		);
		add_submenu_page(
			'elastic-instantsearch',
			esc_html__( 'Autocomplete', 'elastic-instantsearch' ),
			esc_html__( 'Autocomplete', 'elastic-instantsearch' ),
			$this->capability,
			$this->slug,
			array( $this, 'display_page' )
		);
	}

    /**
     * Add settings
     */
	public function add_settings() {
		add_settings_section(
			$this->section,
			null,
			array( $this, 'print_section_settings' ),
			$this->slug
		);

		add_settings_field(
			'es_autocomplete_enabled',
			esc_html__( 'Enable autocomplete', 'elastic-instantsearch' ),
			array( $this, 'autocomplete_enabled_callback' ),
			$this->slug,
			$this->section
		);

		add_settings_field(
			'es_autocomplete_config',
			esc_html__( 'Configuration', 'elastic-instantsearch' ),
			array( $this, 'autocomplete_config_callback' ),
			$this->slug,
			$this->section
		);

		register_setting( $this->option_group, 'es_autocomplete_enabled', array( $this, 'sanitize_autocomplete_enabled' ) );
		register_setting( $this->option_group, 'es_autocomplete_config', array( $this, 'sanitize_autocomplete_config' ) );
	}

	/**
	 * Render autocomplete enabled checkbox
	 */
	public function autocomplete_enabled_callback() {
		$value    = $this->settings->get_autocomplete_enabled();
		$indices  = $this->autocomplete_config->get_form_data();
		$checked  = 'yes' === $value ? 'checked ' : '';
		$disabled = empty( $indices ) ? 'disabled ' : '';
        ?>
		<input type='checkbox' name='es_autocomplete_enabled' value='yes' <?php echo esc_html( $checked . ' ' . $disabled ); ?>/>
        <?php
	}

	/**
     * Sanitize autocomplete
     *
	 * @param string $value Value.
	 * @return string
	 */
	public function sanitize_autocomplete_enabled( $value ) {

		add_settings_error(
			$this->option_group,
			'autocomplete_enabled',
			esc_html__( 'Autocomplete configuration has been saved. Make sure to hit the "re-index" buttons of the different indices that are not indexed yet.', 'elastic-instantsearch' ),
			'updated'
		);

		return 'yes' === $value ? 'yes' : 'no';
	}

    /**
     * Render config form
     */
	public function autocomplete_config_callback() {
		$indices = $this->autocomplete_config->get_form_data();

		require_once dirname( __FILE__ ) . '/partials/page-autocomplete-config.php';
	}

    /**
     * Sanitize form data
     *
     * @param array $values Values.
     */
	public function sanitize_autocomplete_config( $values ) {
		return $this->autocomplete_config->sanitize_form_data( $values );
	}

	/**
	 * Display the page.
	 */
	public function display_page() {
		require_once dirname( __FILE__ ) . '/partials/page-autocomplete.php';
	}

	/**
	 * Display the errors.
	 */
	public function display_errors() {
		settings_errors( $this->option_group );

		if ( defined( 'ES_HIDE_HELP_NOTICES' ) && ES_HIDE_HELP_NOTICES ) {
			return;
		}

		$is_enabled = 'yes' === $this->settings->get_autocomplete_enabled();
		$indices    = $this->autocomplete_config->get_config();

		if ( true === $is_enabled && empty( $indices ) ) {
			/* translators: placeholder contains the URL to the autocomplete configuration page. */
			$message = sprintf( __( 'Please select one or multiple indices on the <a href="%s">Elastic: Autocomplete configuration page</a>.', 'elastic-instantsearch' ), esc_url( admin_url( 'admin.php?page=' . $this->slug ) ) );
			echo '<div class="error notice">
					  <p>' . esc_html__( 'You have enabled the Elastic Autocomplete feature but did not choose any index to search in.', 'elastic-instantsearch' ) . '</p>
					  <p>' . wp_kses_post( $message ) . '</p>
				  </div>';
		}
	}

	/**
	 * Prints the section text.
	 */
	public function print_section_settings() {
		echo '<p>' . esc_html__( 'The autocomplete feature adds a find-as-you-type dropdown menu to your search bar(s).', 'elastic-instantsearch' ) . '</p>';
	}
}
