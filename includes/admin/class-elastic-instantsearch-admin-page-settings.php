<?php
/**
 * Admin settings page
 *
 * @package Elastic_Instantsearch\Admin
 */

/**
 * Elastic_Instantsearch_Admin_Page_Settings
 *
 * @class Elastic_Instantsearch_Admin_Page_Settings
 */
class Elastic_Instantsearch_Admin_Page_Settings {

	/**
     * Slug
     *
	 * @var string
	 */
	private $slug = 'es-account-settings';

	/**
     * Capability
     *
	 * @var string
	 */
	private $capability = 'manage_options';

	/**
     * Section
     *
	 * @var string
	 */
	private $section = 'es_section_settings';

	/**
     * Option Group
     *
	 * @var string
	 */
	private $option_group = 'es_settings';

	/**
     * Plugin.
     *
	 * @var Elastic_Instantsearch
	 */
	private $plugin;

	/**
     * Elastic_Instantsearch_Admin_Page_Settings init.
     *
	 * @param Elastic_Instantsearch $plugin Plugin.
	 */
	public function __construct( Elastic_Instantsearch $plugin ) {
		$this->plugin = $plugin;

		add_action( 'admin_menu', array( $this, 'add_page' ) );
		add_action( 'admin_init', array( $this, 'add_settings' ) );
		add_action( 'admin_notices', array( $this, 'display_errors' ) );

		// Display a link to this page from the plugins page.
		add_filter( 'plugin_action_links_' . ELASTIC_INSTANTSEARCH_PLUGIN_BASENAME, array( $this, 'add_action_links' ) );
	}

	/**
     * Add action links
     *
	 * @param array $links Links.
	 * @return array
	 */
	public function add_action_links( array $links ) {
		return array_merge(
            $links,
            array(
				'<a href="' . esc_url( admin_url( 'admin.php?page=' . $this->slug ) ) . '">' . esc_html__( 'Settings', 'elastic-instantsearch' ) . '</a>',
			)
		);
	}

    /**
     * Add page
     */
	public function add_page() {
		$api = $this->plugin->get_api();
		if ( ! $api->is_reachable() ) {
			// Means this is the only reachable admin page, so make it the default one!
			return add_menu_page(
				'Elastic Search',
				esc_html__( 'Elastic search', 'elastic-instantsearch' ),
				'manage_options',
				$this->slug,
                array( $this, 'display_page' ),
                ELASTIC_LOGO_WHITE
            );
		}

		add_submenu_page(
			'elastic-instantsearch',
			esc_html__( 'Settings', 'elastic-instantsearch' ),
			esc_html__( 'Settings', 'elastic-instantsearch' ),
			$this->capability,
			$this->slug,
			array( $this, 'display_page' )
        );
	}

    /**
     * Add form settings
     */
	public function add_settings() {
		add_settings_section(
			$this->section,
			null,
			array( $this, 'print_section_settings' ),
			$this->slug
		);
        $settings = [
			'es_client_host'       => [
                'label'             => esc_html__( 'Client Host', 'elastic-instantsearch' ),
                'view_callback'     => [ $this, 'client_host_callback' ],
                'register_callback' => [ $this, 'sanitize_client_host' ],
            ],
            'es_client_port'       => [
                'label'             => esc_html__( 'Client Port', 'elastic-instantsearch' ),
                'view_callback'     => [ $this, 'client_port_callback' ],
                'register_callback' => [ $this, 'sanitize_client_port' ],
            ],
            'es_client_scheme'     => [
                'label'             => esc_html__( 'Client Scheme', 'elastic-instantsearch' ),
                'view_callback'     => [ $this, 'client_scheme_callback' ],
                'register_callback' => [ $this, 'sanitize_client_scheme' ],
            ],
            'es_client_user'       => [
                'label'             => esc_html__( 'Client User', 'elastic-instantsearch' ),
                'view_callback'     => [ $this, 'client_user_callback' ],
                'register_callback' => [ $this, 'sanitize_client_user' ],
            ],
            'es_client_pass'       => [
                'label'             => esc_html__( 'Client Password', 'elastic-instantsearch' ),
                'view_callback'     => [ $this, 'client_pass_callback' ],
                'register_callback' => [ $this, 'sanitize_client_password' ],
            ],
            'es_search_client_url' => [
                'label'             => esc_html__( 'Search Client URL', 'elastic-instantsearch' ),
                'view_callback'     => [ $this, 'search_client_url_callback' ],
                'register_callback' => [ $this, 'sanitize_search_client_url' ],
            ],
            'es_index_name_prefix' => [
                'label'             => esc_html__( 'Index Name Prefix', 'elastic-instantsearch' ),
                'view_callback'     => [ $this, 'index_name_prefix_callback' ],
                'register_callback' => [ $this, 'sanitize_index_name_prefix' ],
            ],
        ];

        foreach ( $settings as $name => $setting ) {
            add_settings_field(
                $name,
                $setting['label'],
                $setting['view_callback'],
                $this->slug,
                $this->section
            );
        }
        foreach ( $settings as $name => $setting ) {
            register_setting( $this->option_group, $name, $setting['register_callback'] );
        }
	}

    /**
     * Render client host input
     */
	public function client_host_callback() {

		$settings      = $this->plugin->get_settings();
		$setting       = $settings->get_client_host();
		$disabled_html = $settings->is_client_host_in_config() ? ' disabled' : '';
        ?>
		<input type="text" name="es_client_host" class="regular-text" value="<?php echo esc_attr( $setting ); ?>" <?php echo esc_html( $disabled_html ); ?>/>
		<p class="description" id="home-description"><?php esc_html_e( 'Your Elastic Client Host.', 'elastic-instantsearch' ); ?></p>
        <?php
    }

    /**
     * Render client port input
     */
	public function client_port_callback() {

		$settings      = $this->plugin->get_settings();
		$setting       = $settings->get_client_port();
		$disabled_html = $settings->is_client_port_in_config() ? ' disabled' : '';
        ?>
		<input type="text" name="es_client_port" class="regular-text" value="<?php echo esc_attr( $setting ); ?>" <?php echo esc_html( $disabled_html ); ?>/>
		<p class="description" id="home-description"><?php esc_html_e( 'Your Elastic Client Port.', 'elastic-instantsearch' ); ?></p>
        <?php
    }

    /**
     * Render client scheme input
     */
    public function client_scheme_callback() {
        $settings      = $this->plugin->get_settings();
		$setting       = $settings->get_client_scheme();
		$disabled_html = $settings->is_client_scheme_in_config() ? ' disabled' : '';
        ?>
		<input type="text" name="es_client_scheme" class="regular-text" value="<?php echo esc_attr( $setting ); ?>" <?php echo esc_html( $disabled_html ); ?>/>
		<p class="description" id="home-description"><?php esc_html_e( 'Your Elastic Client Scheme.', 'elastic-instantsearch' ); ?></p>
        <?php
    }

    /**
     * Render client scheme input
     */
    public function client_user_callback() {
        $settings      = $this->plugin->get_settings();
		$setting       = $settings->get_client_user();
		$disabled_html = $settings->is_client_user_in_config() ? ' disabled' : '';
        ?>
		<input type="text" name="es_client_user" autocomplete="off" class="regular-text" value="<?php echo esc_attr( $setting ); ?>" <?php echo esc_html( $disabled_html ); ?>/>
		<p class="description" id="home-description"><?php esc_html_e( 'Your Elastic Client User.', 'elastic-instantsearch' ); ?></p>
        <?php
    }

    /**
     * Render client pass input
     */
	public function client_pass_callback() {
		$settings      = $this->plugin->get_settings();
		$setting       = $settings->get_client_pass();
		$disabled_html = $settings->is_client_pass_in_config() ? ' disabled' : '';
        ?>
		<input type="password" name="es_client_pass" autocomplete="new-password" class="regular-text" value="<?php echo esc_attr( $setting ); ?>" <?php echo esc_html( $disabled_html ); ?>/>
		<p class="description" id="home-description"><?php esc_html_e( 'Your Elastic Client Password.', 'elastic-instantsearch' ); ?></p>
        <?php
    }

    /**
     * Render search client url input
     */
    public function search_client_url_callback() {
		$settings      = $this->plugin->get_settings();
		$setting       = $settings->get_search_client_url();
		$disabled_html = $settings->is_search_client_url_in_config() ? ' disabled' : '';
        ?>
		<input type="text" name="es_search_client_url" class="regular-text" value="<?php echo esc_attr( $setting ); ?>" <?php echo esc_html( $disabled_html ); ?>/>
		<p class="description" id="home-description"><?php esc_html_e( 'Your Elastic Search Client Url.', 'elastic-instantsearch' ); ?></p>
        <?php
    }

    /**
     * Render name prefix
     */
	public function index_name_prefix_callback() {
		$settings          = $this->plugin->get_settings();
		$index_name_prefix = $settings->get_index_name_prefix();
		$disabled_html     = $settings->is_index_name_prefix_in_config() ? ' disabled' : '';
        ?>
		<input type="text" name="es_index_name_prefix" value="<?php echo esc_attr( $index_name_prefix ); ?>" <?php echo esc_html( $disabled_html ); ?>/>
		<p class="description" id="home-description"><?php esc_html_e( 'This prefix will be prepended to your index names.', 'elastic-instantsearch' ); ?></p>
        <?php
	}

    /**
     * Sanitize client host
     *
     * @param string $value Value.
     */
	public function sanitize_client_host( $value ) {
		if ( $this->plugin->get_settings()->is_client_host_in_config() ) {
			$value = $this->plugin->get_settings()->get_client_host();
		}
		$value = sanitize_text_field( $value );

		if ( empty( $value ) ) {
			add_settings_error(
				$this->option_group,
				'empty',
				esc_html__( 'Client host should not be empty.', 'elastic-instantsearch' )
			);

		}

		return $value;
    }

    /**
     * Sanitize client port
     *
     * @param string $value Value.
     */
	public function sanitize_client_port( $value ) {
		if ( $this->plugin->get_settings()->is_client_port_in_config() ) {
			$value = $this->plugin->get_settings()->get_client_port();
		}
		$value = sanitize_text_field( $value );

		return $value;
    }

    /**
     * Sanitize client scheme
     *
     * @param string $value Value.
     */
    public function sanitize_client_scheme( $value ) {
		if ( $this->plugin->get_settings()->is_client_scheme_in_config() ) {
			$value = $this->plugin->get_settings()->get_client_scheme();
		}
		$value = sanitize_text_field( $value );

		return $value;
    }

    /**
     * Sanitize client user
     *
     * @param string $value Value.
     */
    public function sanitize_client_user( $value ) {
		if ( $this->plugin->get_settings()->is_client_user_in_config() ) {
			$value = $this->plugin->get_settings()->get_client_user();
		}
		$value = sanitize_text_field( $value );

		return $value;
    }

    /**
     * Sanitize search client url
     *
     * @param string $value Value.
     */
	public function sanitize_search_client_url( $value ) {
		if ( $this->plugin->get_settings()->is_search_client_url_in_config() ) {
			$value = $this->plugin->get_settings()->get_search_client_url();
		}
		$value = sanitize_text_field( $value );

		if ( empty( $value ) ) {
			add_settings_error(
				$this->option_group,
				'empty',
				esc_html__( 'Search client url should not be empty.', 'elastic-instantsearch' )
			);
		}

		return $value;
	}

    /**
     * Sanitize client password and check connection
     *
     * @param string $value Value.
     */
	public function sanitize_client_password( $value ) {
		if ( $this->plugin->get_settings()->is_client_pass_in_config() ) {
			$value = $this->plugin->get_settings()->get_client_pass();
		}
        $value = sanitize_text_field( $value );

		if ( empty( $value ) ) {
			add_settings_error(
				$this->option_group,
				'empty',
				esc_html__( 'Password should not be empty', 'elastic-instantsearch' )
			);
		}
		$errors = get_settings_errors( $this->option_group );
		if ( ! empty( $errors ) ) {
			return $value;
		}

		$settings = $this->plugin->get_settings();

		try {
			$info = Elastic_Instantsearch_API::assert_valid_credentials(
                $settings->get_client_host(),
                $settings->get_client_port(),
                $settings->get_client_scheme(),
                $settings->get_client_user(),
                $value
            );
            $settings->set_api_is_reachable( true );
            add_settings_error(
                $this->option_group,
                'connection_success',
                esc_html__( 'We succesfully managed to connect with Elastic Server!', 'elastic-instantsearch' ),
                'updated'
            );
		} catch ( Exception $exception ) {
			add_settings_error(
				$this->option_group,
				'login_exception',
				$exception->getMessage()
            );
            add_settings_error(
				$this->option_group,
				'no_connection',
				esc_html__(
					'We were unable to connect with Elastic Server!',
					'elastic-instantsearch'
				)
            );
            $settings->set_api_is_reachable( false );
		}

		return $value;
	}

	/**
     * Check if index name prefix is valid
     *
	 * @param string $index_name_prefix Prefix.
	 *
	 * @return string
	 */
	public function is_valid_index_name_prefix( $index_name_prefix ) {
		$to_validate = str_replace( '_', '', $index_name_prefix );

		return ctype_alnum( $to_validate );
	}

	/**
     * Sanitize index name prefix
     *
	 * @param string $value Prefix.
	 * @return array
	 */
	public function sanitize_index_name_prefix( $value ) {
		if ( $this->plugin->get_settings()->is_index_name_prefix_in_config() ) {
			$value = $this->plugin->get_settings()->get_index_name_prefix();
		}

		if ( $this->is_valid_index_name_prefix( $value ) ) {
			return $value;
		}

		add_settings_error(
			$this->option_group,
			'wrong_prefix',
			esc_html__( 'Indices prefix can only contain alphanumeric characters and underscores.', 'elastic-instantsearch' )
		);

		$value = get_option( 'es_index_name_prefix' );

		return $this->is_valid_index_name_prefix( $value ) ? $value : 'wp_';
	}


	/**
	 * Display the page.
	 */
	public function display_page() {
		require_once dirname( __FILE__ ) . '/partials/form-options.php';
	}

    /**
     * Display Errors
     */
	public function display_errors() {
		settings_errors( $this->option_group );
	}

    /**
     * Print Info Section
     */
	public function print_section_settings() {
		echo '<p>' . esc_html__( 'Configure here your Elastic Search credentials', 'elastic-instantsearch' ) . '</p>';
		echo '<p>' . esc_html__( 'Once you provide your credentials, this plugin will be able to securely communicate with Elastic servers.', 'elastic-instantsearch' ) . ' ' . esc_html__( 'We ensure your information is correct by testing them against the Elastic Search server upon save.', 'elastic-instantsearch' ) . '</p>';
	}
}
