<?php
/**
 * Admin class
 *
 * @package Elastic_Instantsearch/Admin
 */

/**
 * Elastic_Instantsearch_Admin
 *
 * @class Elastic_Instantsearch
 */
class Elastic_Instantsearch_Admin {

	/**
     * Plugin
     *
	 * @var Elastic_Instantsearch
	 */
	private $plugin;

	/**
     * Elastic_Instantsearch init
     *
	 * @param Elastic_Instantsearch $plugin Plugin.
	 */
	public function __construct( Elastic_Instantsearch $plugin ) {
		$this->plugin = $plugin;

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		$api = $plugin->get_api();
		if ( $api->is_reachable() ) {
			new Elastic_Instantsearch_Admin_Page_Autocomplete( $plugin->get_settings(), $this->plugin->get_autocomplete_config() );
			new Elastic_Instantsearch_Admin_Page_Native_Search( $plugin );

			add_action( 'wp_ajax_es_re_index', array( $this, 're_index' ) );
			add_action( 'wp_ajax_es_push_settings', array( $this, 'push_settings' ) );

			if ( isset( $_GET['page'] ) && substr( (string) $_GET['page'], 0, 7 ) === 'elastic-instantsearch' ) { // phpcs:ignore
				add_action( 'admin_notices', array( $this, 'display_reindexing_notices' ) );
			}
		}

		new Elastic_Instantsearch_Admin_Page_Settings( $plugin );

	}

    /**
     * Enqueue styles.
     */
	public function enqueue_styles() {
		wp_enqueue_style( 'elastic-instantsearch-admin', plugin_dir_url( __FILE__ ) . 'css/elastic-instantsearch-admin.css', array(), ELASTIC_INSTANTSEARCH_VERSION );
	}

	/**
	 * Enqueue scripts.
	 */
	public function enqueue_scripts() {
		wp_enqueue_script(
            'elastic-instantsearch-admin',
            plugin_dir_url( __FILE__ ) . 'js/elastic-instantsearch-admin.js',
            array( 'jquery', 'jquery-ui-sortable' ),
            ELASTIC_INSTANTSEARCH_VERSION,
            true
        );
		wp_enqueue_script(
            'elastic-instantsearch-admin-reindex-button',
            plugin_dir_url( __FILE__ ) . 'js/reindex-button.js',
            array( 'jquery' ),
            ELASTIC_INSTANTSEARCH_VERSION,
            true
        );
		wp_enqueue_script(
            'elastic-instantsearch-admin-push-settings-button',
            plugin_dir_url( __FILE__ ) . 'js/push-settings-button.js',
            array( 'jquery' ),
            ELASTIC_INSTANTSEARCH_VERSION,
            true
        );
	}

    /**
     * Display reindex notices
     */
	public function display_reindexing_notices() {
		$indices = $this->plugin->get_indices(
			array(
				'enabled' => true,
			)
		);
		foreach ( $indices as $index ) {
			if ( $index->exists() ) {
				continue;
			}

	        ?>
        <div class="error">
            <p>For Elastic search to work properly, you need to index: <strong><?php echo esc_html( $index->get_admin_name() ); ?></strong></p>
            <p><button class="es-reindex-button button button-primary" data-index="<?php echo esc_attr( $index->get_id() ); ?>">Index now</button></p>
        </div>
            <?php
		}
	}

    /**
     * Re index
     *
     * @throws RuntimeException Runtime exception.
     * @throws \Exception Exception.
     */
	public function re_index() {
		try {
			if ( ! isset( $_POST['index_id'] ) ) { // phpcs:ignore
				throw new RuntimeException( 'Index ID should be provided.' );
			}
			$index_id = (string) $_POST['index_id']; // phpcs:ignore

			if ( ! isset( $_POST['p'] ) ) { // phpcs:ignore
				throw new RuntimeException( 'Page should be provided.' );
			}
			$page = (int) $_POST['p'];

			$index = $this->plugin->get_index( $index_id );
			if ( null === $index ) {
				throw new RuntimeException( sprintf( 'Index named %s does not exist.', $index_id ) );
			}

			$total_pages = $index->get_re_index_max_num_pages();

			ob_start();
			if ( $page <= $total_pages || 0 === $total_pages ) {
				$index->re_index( $page );
			}
			ob_end_clean();

			$response = array(
				'totalPagesCount' => $total_pages,
				'finished'        => $page >= $total_pages,
			);

			wp_send_json( $response );
		} catch ( \Exception $exception ) {
			echo esc_html( $exception->getMessage() );
			throw $exception;
		}
	}

    /**
     * Push settings
     *
     * @throws RuntimeException Runtime exception.
     * @throws \Exception Exception.
     */
	public function push_settings() {
		try {
			if ( ! isset( $_POST['index_id'] ) ) { // phpcs:ignore
				throw new RuntimeException( 'index_id should be provided.' );
			}
			$index_id = (string) $_POST['index_id']; // phpcs:ignore

			$index = $this->plugin->get_index( $index_id );
			if ( null === $index ) {
				throw new RuntimeException( sprintf( 'Index named %s does not exist.', $index_id ) );
			}

			$index->push_settings();

			$response = array(
				'success' => true,
			);
			wp_send_json( $response );
		} catch ( \Exception $exception ) {
			echo esc_html( $exception->getMessage() );
			throw $exception;
		}
	}
}
