<?php
/**
 * Override search config template
 *
 * @package Elastic_Instantsearch/Admin
 */

?>
<div class="input-radio">
	<label>
        <input
            type="radio"
            value="native"
            name="es_override_native_search"
            <?php if ( 'native' === $value ) : ?>
            checked
            <?php endif; ?>
        >
		<?php esc_html_e( 'Do not use Elastic', 'elastic-instantsearch' ); ?>
	</label>
	<div class="radio-info">
		Do not use Elastic for searching at all.
		This is only a valid option if you wish to search on your content from another website.
	</div>

	<label>
        <input
            type="radio"
            value="backend"
            name="es_override_native_search" 
            <?php if ( 'backend' === $value ) : ?>
            checked
            <?php endif; ?>
        >
		<?php esc_html_e( 'Use Elastic in the backend', 'elastic-instantsearch' ); ?>
	</label>
	<div class="radio-info">
		With this option WordPress search will be powered by Elastic behind the scenes.
	</div>
</div>
