<?php
/**
 * Admin Native Search class
 *
 * @package Elastic_Instantsearch/Admin
 */

/**
 * Elastic_Instantsearch_Admin_Page_Native_Search
 *
 * @class Elastic_Instantsearch_Admin_Page_Native_Search
 */
class Elastic_Instantsearch_Admin_Page_Native_Search {

	/**
     * Slug
     *
	 * @var string
	 */
	private $slug = 'es-search-page';

	/**
     * Capability
     *
	 * @var string
	 */
	private $capability = 'manage_options';

	/**
     * Section
     *
	 * @var string
	 */
	private $section = 'es_section_native_search';

	/**
     * Option Group
     *
	 * @var string
	 */
	private $option_group = 'es_native_search';

	/**
     * Plugin
     *
	 * @var Elastic_Instantsearch
	 */
	private $plugin;

	/**
     * Init.
     *
	 * @param Elastic_Instantsearch $plugin Plugin.
	 */
	public function __construct( Elastic_Instantsearch $plugin ) {
		$this->plugin = $plugin;

		add_action( 'admin_menu', array( $this, 'add_page' ) );
		add_action( 'admin_init', array( $this, 'add_settings' ) );
		add_action( 'admin_notices', array( $this, 'display_errors' ) );
	}

    /**
     * Add page.
     */
	public function add_page() {
		add_submenu_page(
			'elastic-instantsearch',
			esc_html__( 'Search Page', 'elastic-instantsearch' ),
			esc_html__( 'Search Page', 'elastic-instantsearch' ),
			$this->capability,
			$this->slug,
			array( $this, 'display_page' )
		);
	}

    /**
     * Add settings
     */
	public function add_settings() {
		add_settings_section(
			$this->section,
			null,
			array( $this, 'print_section_settings' ),
			$this->slug
		);

		add_settings_field(
			'es_override_native_search',
			esc_html__( 'Search results', 'elastic-instantsearch' ),
			array( $this, 'override_native_search_callback' ),
			$this->slug,
			$this->section
		);

		register_setting( $this->option_group, 'es_override_native_search', array( $this, 'sanitize_override_native_search' ) );
	}

    /**
     * Render native search form.
     */
	public function override_native_search_callback() {
		$value = $this->plugin->get_settings()->get_override_native_search();

		require_once dirname( __FILE__ ) . '/partials/form-override-search-option.php';
	}

	/**
     * Sanitize native search
     *
	 * @param string $value Value.
	 * @return string
	 */
	public function sanitize_override_native_search( $value ) {

		if ( 'backend' === $value ) {
			add_settings_error(
				$this->option_group,
				'native_search_enabled',
				esc_html__( 'WordPress search is now based on Elastic!', 'elastic-instantsearch' ),
				'updated'
			);
		} else {
			$value = 'native';
			add_settings_error(
				$this->option_group,
				'native_search_disabled',
				esc_html__( 'You chose to keep the WordPress native search instead of Elastic. If you are using the autocomplete feature of the plugin we highly recommend you turn Elastic search on instead of the WordPress native search.', 'elastic-instantsearch' ),
				'updated'
			);
		}

		return $value;
	}

	/**
	 * Display the page.
	 */
	public function display_page() {
		require_once dirname( __FILE__ ) . '/partials/page-search.php';
	}

	/**
	 * Display the errors.
	 */
	public function display_errors() {
		settings_errors( $this->option_group );

		if ( defined( 'ES_HIDE_HELP_NOTICES' ) && ES_HIDE_HELP_NOTICES ) {
			return;
		}

		$settings = $this->plugin->get_settings();

		if ( ! $settings->should_override_search_in_backend() && ! $settings->should_override_search_with_instantsearch() ) {
			return;
		}

		$searchable_posts_index = $this->plugin->get_index( 'searchable_posts' );
		if ( false === $searchable_posts_index->is_enabled() && isset( $_GET['page'] ) && $_GET['page'] === $this->slug ) { // phpcs:ignore
			/* translators: placeholder contains the link to the indexing page. */
			$message = sprintf( __( 'Searchable posts index needs to be checked on the <a href="%s">Elastic: Indexing page</a> for the search results to be powered by Elastic.', 'elastic-instantsearch' ), esc_url( admin_url( 'admin.php?page=es-indexing' ) ) );
			echo '<div class="error notice">
					  <p>' . wp_kses_post( $message ) . '</p>
				  </div>';
		}
	}

	/**
	 * Prints the section text.
	 */
	public function print_section_settings() {
		echo '<p>' . esc_html__( 'By enabling this plugin to override the native WordPress search, your search results will be powered by Elastic search.', 'elastic-instantsearch' ) . '</p>';

		// todo: replace this with a check on the searchable_posts_index.
		$indices = $this->plugin->get_indices(
			array(
				'enabled'  => true,
				'contains' => 'posts',
			)
		);

		if ( empty( $indices ) ) {
			echo '<div class="error-message">' .
					esc_html( __( 'You have no index containing only posts yet. Please index some content on the `Indexing` page.', 'elastic-instantsearch' ) ) .
					'</div>';
		}
	}
}
