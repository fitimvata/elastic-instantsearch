<?php
/**
 * Template loader
 *
 * @package Elastic_Instantsearch
 */

/**
 * Elastic Instantsearch Template Loader
 *
 * @class Elastic_Instantsearch_Template_Loader
 */
class Elastic_Instantsearch_Template_Loader {

	/**
     * Plugin instance.
     *
	 * @var Elastic_Instantsearch
	 */
	private $plugin;

	/**
	 * Elastic_Instantsearch_Template_Loader constructor.
	 *
	 * @param Elastic_Instantsearch $plugin Plugin instance.
	 */
	public function __construct( Elastic_Instantsearch $plugin ) {
		$this->plugin = $plugin;

		// Inject Elastic configuration in a JavaScript variable.
		add_filter( 'wp_head', array( $this, 'load_es_config' ) );

		// Listen for native templates to override.
		add_filter( 'template_include', array( $this, 'template_loader' ) );

		// Load autocomplete.js search experience if its enabled.
		if ( $this->should_load_autocomplete() ) {
			add_filter( 'wp_enqueue_scripts', array( $this, 'enqueue_autocomplete_scripts' ) );
			add_filter( 'wp_footer', array( $this, 'load_autocomplete_template' ), PHP_INT_MAX );
		}
	}

    /**
     * Load elastic config
     */
	public function load_es_config() {
		$settings            = $this->plugin->get_settings();
		$autocomplete_config = $this->plugin->get_autocomplete_config();

		$config = array(
			'debug'         => defined( 'WP_DEBUG' ) && WP_DEBUG,
			'search_client' => $settings->get_search_client_url(),
			'query'         => isset( $_GET['s'] ) ? wp_unslash( $_GET['s'] ) : '', // phpcs:ignore
			'autocomplete'  => array(
				'sources'        => $autocomplete_config->get_config(),
				'input_selector' => (string) apply_filters( 'es_autocomplete_input_selector', "input[name='s']" ),
			),
			'indices'       => array(),
		);

		// Inject all the indices into the config to ease instantsearch.js integrations.
		$indices = $this->plugin->get_indices(
			array(
				'enabled' => true,
			)
		);
		foreach ( $indices as $index ) {
			$config['indices'][ $index->get_id() ] = $index->to_array();
		}

		// Give developers a last chance to alter the configuration.
		$config = (array) apply_filters( 'es_config', $config );

		echo '<script type="text/javascript">var elastic_instantsearch = ' . wp_json_encode( $config ) . ';</script>';
	}

    /**
     * Should load autocompete
     */
	private function should_load_autocomplete() {
		$settings     = $this->plugin->get_settings();
		$autocomplete = $this->plugin->get_autocomplete_config();

		if ( null === $autocomplete ) {
			// The user has not provided his credentials yet.
			return false;
		}

		$config = $autocomplete->get_config();
		if ( 'yes' !== $settings->get_autocomplete_enabled() ) {
			return false;
		}

		return ! empty( $config );
	}

	/**
	 * Enqueue Elastic autocomplete.js scripts.
	 */
	public function enqueue_autocomplete_scripts() {
		wp_enqueue_style( 'es-autocomplete' );
		wp_enqueue_script( 'es-autocomplete' );
		wp_enqueue_script( 'es-autocomplete-source' );
        do_action( 'es_autocomplete_scripts' );
	}

	/**
	 * Load a template.
	 *
	 * Handles template usage so that we can use our own templates instead of the themes.
	 *
	 * Templates are in the 'templates' folder. es looks for theme.
	 * overrides in /your-theme/elastic-instantsearch/ by default.
	 *
	 * @param mixed $template Template.
	 *
	 * @return string
	 */
	public function template_loader( $template ) {
		$settings = $this->plugin->get_settings();
		if ( is_search() && $settings->should_override_search_with_instantsearch() ) {

			return $this->load_instantsearch_template();
		}

		return $template;
	}

	/**
     * TODO
     * Load instant search template
     *
	 * @return string
	 */
	public function load_instantsearch_template() {
        // phpcs:disable
		// add_action(
        //     'wp_enqueue_scripts',
        //     function () {
		// 		// Enqueue the instantsearch.js default styles.
		// 		wp_enqueue_style( 'es-instantsearch' );

		// 		// Enqueue the instantsearch.js library.
		// 		wp_enqueue_script( 'es-instantsearch' );

		// 		// Allow users to easily enqueue custom styles and scripts.
		// 		do_action( 'es_instantsearch_scripts' );
		// 	}
		// );
        // return $this->locate_template( 'instantsearch.php' );
        // phpcs:enable
        return '';
	}

	/**
     * Load autocomplete template
	 */
	public function load_autocomplete_template() {
		require $this->locate_template( 'autocomplete.php' );
	}

	/**
     * Locate template
     *
	 * @param string $file File name.
	 * @return string
	 */
	private function locate_template( $file ) {
		$locations[] = $file;

		$templates_path = $this->plugin->get_templates_path();
		if ( 'elastic-instantsearch/' !== $templates_path ) {
			$locations[] = 'elastic-instantsearch/' . $file;
		}

		$locations[] = $templates_path . $file;

		$locations = (array) apply_filters( 'es_template_locations', $locations, $file );

		$template = locate_template( array_unique( $locations ) );

		$default_template = (string) apply_filters( 'es_default_template', $this->plugin->get_path() . '/templates/' . $file, $file );

		return $template ? $template : $default_template;
	}
}
