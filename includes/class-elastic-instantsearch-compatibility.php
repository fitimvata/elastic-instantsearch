<?php
/**
 * Compatibility
 *
 * @package Elastic_Instantsearch
 */

/**
 * Elastic_Instantsearch_Compatibility
 *
 * @class Elastic_Instantsearch_Compatibility
 */
class Elastic_Instantsearch_Compatibility {

    /**
     * Current lang
     *
     * @var string
     */
	private $current_language;

    /**
     * Elastic_Instantsearch_Compatibility init
     */
	public function __construct() {
		add_action( 'es_before_get_record', array( $this, 'register_vc_shortcodes' ) );
		add_action( 'es_before_get_record', array( $this, 'enable_yoast_frontend' ) );
		add_action( 'es_before_get_record', array( $this, 'wpml_switch_language' ) );
		add_action( 'es_after_get_record', array( $this, 'wpml_switch_back_language' ) );
	}

    /**
     * Enable yoast frontend
     */
	public function enable_yoast_frontend() {
		if ( class_exists( 'WPSEO_Frontend' ) && method_exists( 'WPSEO_Frontend', 'get_instance' ) ) {
			WPSEO_Frontend::get_instance();
		}
	}

    /**
     * Register vc shortcodes
     */
	public function register_vc_shortcodes() {
		if ( class_exists( 'WPBMap' ) && method_exists( 'WPBMap', 'addAllMappedShortcodes' ) ) {
			WPBMap::addAllMappedShortcodes();
		}
	}

    /**
     * Switch lang
     *
     * @param WP_Post $post Post.
     */
	public function wpml_switch_language( $post ) {
		if ( ! $post instanceof WP_Post || ! $this->is_wpml_enabled() ) {
			return;
		}

		global $sitepress;
		$lang_info              = wpml_get_language_information( null, $post->ID );
		$this->current_language = $sitepress->get_current_language();
		$sitepress->switch_lang( $lang_info['language_code'] );
	}

    /**
     * Switch back lang
     *
     * @param WP_Post $post Post.
     */
	public function wpml_switch_back_language( $post ) {
		if ( ! $post instanceof WP_Post || ! $this->is_wpml_enabled() ) {
			return;
		}

		global $sitepress;

		$sitepress->switch_lang( $this->current_language );
	}

	/**
     * Is wpml enable
     *
	 * @return bool
	 */
	private function is_wpml_enabled() {
		return function_exists( 'icl_object_id' ) && ! class_exists( 'Polylang' );
	}
}
