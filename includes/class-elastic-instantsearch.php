<?php
/**
 * Elastic instantsearch setup
 *
 * @package Elastic_Instantsearch
 */

/**
 * Main Elastic instantsearch class
 *
 * @class Elastic_Instantsearch
 */
class Elastic_Instantsearch {
    const NAME = 'elastic-instantsearch';

	/**
     * Holds current plugin instance
     *
	 * @var Elastic_Instantsearch
	 */
    static private $instance;

    /**
     * API
     *
	 * @var Elastic_Instantsearch_API
	 */
	protected $api;

	/**
     * Settings
     *
	 * @var Elastic_Instantsearch_Settings
	 */
    private $settings;

    /**
     * Autocomplete Config
     *
	 * @var Elastic_Instantsearch_Autocomplete_Config
	 */
	private $autocomplete_config;

    /**
     * Indices
     *
	 * @var array
	 */
    private $indices;

    /**
     * Changes watchers
     *
	 * @var array
	 */
	private $changes_watchers;

    /**
     * Template Loader
     *
	 * @var Elastic_Instantsearch_Template_Loader
	 */
    private $template_loader;

	/**
     * Compatibility
     *
	 * @var Elastic_Instantsearch_Compatibility
	 */
	private $compatibility;

    /**
     * Returns current plugin instance
     *
	 * @return Elastic_Instantsearch
	 */
	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new Elastic_Instantsearch();
		}
		return self::$instance;
	}

    /**
     * Loads the plugin
     */
    public function __construct() {
        // Register the assets so that they can be used in other plugins outside of the context of the core features.
		add_action( 'wp_enqueue_scripts', array( $this, 'register_assets' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'register_assets' ) );

        $this->settings = new Elastic_Instantsearch_Settings();

        $this->api = new Elastic_Instantsearch_API( $this->settings );

        $this->compatibility = new Elastic_Instantsearch_Compatibility();

        add_action( 'init', [ $this, 'load' ], 20 );
    }

    /**
     * Load plugin
     */
    public function load() {
        if ( $this->api->is_reachable() ) {
            $this->load_indices();
            $this->override_wordpress_search();
            $this->autocomplete_config = new Elastic_Instantsearch_Autocomplete_Config( $this );
            $this->template_loader     = new Elastic_Instantsearch_Template_Loader( $this );
        }

        		// Load admin or public part of the plugin.
		if ( is_admin() ) {
			new Elastic_Instantsearch_Admin( $this );
		}
    }

    /**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @return    string    The name of the plugin.
	 */
	public function get_name() {
		return self::NAME;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return ELASTIC_INSTANTSEARCH_VERSION;
	}

	/**
     * Get API
     *
	 * @return Elastic_Instantsearch_API
	 */
	public function get_api() {
		return $this->api;
	}

	/**
     * Get settings
     *
	 * @return Elastic_Instantsearch_Settings
	 */
	public function get_settings() {
		return $this->settings;
    }


    /**
	 * Replaces native WordPress search results by Elastic_Instantsearch ranked results.
	 */
	private function override_wordpress_search() {
		// Do not override native search if the feature is not enabled.
		if ( ! $this->settings->should_override_search_in_backend() ) {
			return;
		}

		$index_id = $this->settings->get_native_search_index_id();
		$index    = $this->get_index( $index_id );

		if ( null === $index ) {
			return;
		}

		new Elastic_Instantsearch_Search( $index );
    }

    /**
     * Get autocomplete config
     *
	 * @return Elastic_Instantsearch_Autocomplete_Config
	 */
	public function get_autocomplete_config() {
		return $this->autocomplete_config;
    }

    /**
     * Register assets
     */
    public function register_assets() {
        $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		// CSS.
		wp_register_style(
            'es-autocomplete',
            plugin_dir_url( __FILE__ ) . '../dist/css/app.css',
            array(),
            ELASTIC_INSTANTSEARCH_VERSION,
            'screen'
        );

		// JS.
		wp_register_script(
            'es-autocomplete',
            plugin_dir_url( __FILE__ ) . '../dist/js/autocomplete' . $suffix . '.js',
            array( 'jquery', 'underscore', 'wp-util' ),
            ELASTIC_INSTANTSEARCH_VERSION,
            true
        );
        wp_register_script(
            'es-autocomplete-source',
            plugin_dir_url( __FILE__ ) . '../dist/js/app' . $suffix . '.js',
            array( 'es-autocomplete' ),
            ELASTIC_INSTANTSEARCH_VERSION,
            true
        );
	}

    /**
	 * Load indices
	 */
	public function load_indices() {
        $synced_indices_ids = $this->settings->get_synced_indices_ids();
        $client             = $this->get_api()->get_client();
        $index_name_prefix  = $this->settings->get_index_name_prefix();
        $this->indices      = [];

		// Add a searchable posts index.
		$searchable_post_types = get_post_types( [ 'exclude_from_search' => false ], 'names' );
		$searchable_post_types = (array) apply_filters( 'es_searchable_post_types', $searchable_post_types );
		$this->indices[]       = new Elastic_Instantsearch_Searchable_Posts_Index( $searchable_post_types );

		// Add one posts index per post type.
		$post_types = get_post_types();

		$post_types_blacklist = $this->settings->get_post_types_blacklist();
		foreach ( $post_types as $post_type ) {
			// Skip blacklisted post types.
			if ( in_array( $post_type, $post_types_blacklist, true ) ) {
				continue;
			}

			$this->indices[] = new Elastic_Instantsearch_Posts_Index( $post_type );
		}

		// Add one terms index per taxonomy.
		$taxonomies           = get_taxonomies();
		$taxonomies_blacklist = $this->settings->get_taxonomies_blacklist();
		foreach ( $taxonomies as $taxonomy ) {
			// Skip blacklisted post types.
			if ( in_array( $taxonomy, $taxonomies_blacklist, true ) ) {
				continue;
			}

			$this->indices[] = new Elastic_Instantsearch_Terms_Index( $taxonomy );
		}

		// Add the users index.
		$this->indices[] = new Elastic_Instantsearch_Users_Index();

		// Allow developers to filter the indices.
		$this->indices = (array) apply_filters( 'es_indices', $this->indices );

		foreach ( $this->indices as $index ) {
			$index->set_name_prefix( $index_name_prefix );
			$index->set_client( $client );

			if ( in_array( $index->get_id(), $synced_indices_ids ) ) {  // phpcs:disable
				$index->set_enabled( true );

				if ( $index->contains_only( 'posts' ) ) {
					$this->changes_watchers[] = new Elastic_Instantsearch_Post_Changes_Watcher( $index );
                }elseif ( $index->contains_only( 'terms' ) ) {
					$this->changes_watchers[] = new Elastic_Instantsearch_Term_Changes_Watcher( $index );
                } elseif ( $index->contains_only( 'users' ) ) {
					$this->changes_watchers[] = new Elastic_Instantsearch_User_Changes_Watcher( $index );
                }
			}
		}

		$this->changes_watchers = (array) apply_filters( 'es_changes_watchers', $this->changes_watchers );

		foreach ( $this->changes_watchers as $watcher ) {
			$watcher->watch();
		}
    }

    /**
     * Get indices
     *
	 * @param array $args Args.
	 * @return array
	 */
	public function get_indices( array $args = array() ) {
		if ( empty( $args ) ) {
			return $this->indices;
		}

		$indices = $this->indices;

		if ( isset( $args['enabled'] ) && true === $args['enabled'] ) {
			$indices = array_filter(
                $indices,
                function( $index ) {
					return $index->is_enabled();
				}
			);
		}

		if ( isset( $args['contains'] ) ) {
			$contains = (string) $args['contains'];
			$indices  = array_filter(
                $indices,
                function( $index ) use ( $contains ) {
					return $index->contains_only( $contains );
				}
			);
		}

		return $indices;
    }

    /**
     * Get index instance
     *
	 * @param string $index_id Index id.
	 * @return Elastic_Instantsearch_Index|null
	 */
	public function get_index( $index_id ) {
		foreach ( $this->indices as $index ) {
			if ( $index_id === $index->get_id() ) {
				return $index;
			}
		}

		return null;
    }

	/**
	 * Get the plugin path.
	 *
	 * @return string
	 */
	public function get_path() {
		return untrailingslashit( ELASTIC_INSTANTSEARCH_PATH );
	}

	/**
     * Get templates path
     *
	 * @return string
	 */
	public function get_templates_path() {
		return (string) apply_filters( 'es_templates_path', 'elastic-instantsearch/' );
	}

	/**
     * Get template loader
     *
	 * @return Elastic_Instantsearch_Template_Loader
	 */
	public function get_template_loader() {
		return $this->template_loader;
    }
}
