=== Elastic Instantsearch ===
Contributors: fitimvata
Tags: search, Elastic Search, elasticsearch, autocomplete, suggest, ajax search, custom search
Requires at least: 4.4
Tested up to: 5.0.3
Stable tag: 0.1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Bring power of elastic search into your wordpress site. Make search a better user experience.

== Description ==

The plugin provides relevant search results in milliseconds, ensuring that your users can find your best posts 
at the speed of thought. It also comes with full power of elasticsearch.

= Getting started guide =
Once you have installed the plugin, you can follow the step by step guide provided here: <url>

= BUILT BY DEVELOPERS FOR DEVELOPERS =
Elastic Instantsearch is also completely configurable and fully extensible by means of WordPress filters and hooks, letting you build a custom search experience or theme.

Want to see out how we did it?

Check it out here: [desc](<url>)


== Installation ==

= Minimum Requirements =

* PHP version 7.0 or greater
* Requires WordPress 4.4+


= Automatic installation =

Install from wordpress dashboard by searching `Elastic Instantsearch`

= Manual installation =

The manual installation method involves downloading our search plugin and uploading it to your webserver via your favourite FTP application. The WordPress codex contains [instructions on how to do this here](https://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation).

= Updating =

Automatic updates should work like a charm; as always though, ensure you backup your site just in case.

== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 0.1.0 =
* First public release.
