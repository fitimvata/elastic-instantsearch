const mix = require('laravel-mix');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

mix.setPublicPath('dist')
  // HACK to fix problems with url in css files
  .setResourceRoot('../../dist/')
  .disableNotifications()
  .webpackConfig((webpack) => ({
    plugins: [
      // new BundleAnalyzerPlugin({
      //   analyzerHost: '192.168.1.112'
      // }),
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      })
    ],
    externals: {
      jquery: 'jQuery'
    }
  }))
  .copy('node_modules/autocomplete.js/dist/autocomplete.js', 'dist/js/')
  .copy('node_modules/autocomplete.js/dist/autocomplete.min.js', 'dist/js/')
  .sass('assets/sass/app.scss', 'dist/css')



if (mix.inProduction()) {
  mix.js('assets/js/app.js', 'dist/js/app.min.js')
  .version()
} else {
  mix.js('assets/js/app.js', 'dist/js')
}